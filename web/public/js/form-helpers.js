/**
 * Created by nursultan on 6/2/14.
 */
$(function () {

    $.datepicker.regional['kz'] = {
        closeText: 'Жабу',
        prevText: '&#x3C;Алдыңғы',
        nextText: 'Келесі&#x3E;',
        currentText: 'Бүгін',
        monthNames: ['Қаңтар','Ақпан','Наурыз','Сәуір','Мамыр','Маусым',
            'Шілде','Тамыз','Қыркүйек','Қазан','Қараша','Желтоқсан'],
        monthNamesShort: ['Қаң','Ақп','Нау','Сәу','Мам','Мау',
            'Шіл','Там','Қыр','Қаз','Қар','Жел'],
        dayNames: ['Жексенбі','Дүйсенбі','Сейсенбі','Сәрсенбі','Бейсенбі','Жұма','Сенбі'],
        dayNamesShort: ['жкс','дсн','ссн','срс','бсн','жма','снб'],
        dayNamesMin: ['Жк','Дс','Сс','Ср','Бс','Жм','Сн'],
        weekHeader: 'Не',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};


    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3C;Пред',
        nextText: 'След&#x3E;',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
            'Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        weekHeader: 'Нед',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};

    $.datepicker.setDefaults($.datepicker.regional[_locale]);

    $('.date-in-out').datepicker({
        showOn: 'button',
        buttonImageOnly: true,
        buttonText: '',
        minDate: new Date(),
        dateFormat: "yy-mm-dd"
    });

//  Далее не наш код
    $('#to_date').datepicker({
        dateFormat: "dd.mm.yy"
    });
    $('#from_date').datepicker({
        dateFormat: "dd.mm.yy"
    });

    $('.ui-datepicker-trigger').addClass('b-datepicker-icon');
});
