jQuery.fn.slider_simple = function (settings) {
    var config = {
        speed: 300
    };
    if (settings) {
        $.extend(config, settings);
    }


    var click = true;
    var slide = $(this);
    var count = slide.find('li').length;
    var slide_li_width, slide_ul_width, slide_max_margin, slide_new_margin = 0;


    slide_li_width = slide.find('li').width();
    slide.find('ul').width(count * slide_li_width);
    slide_ul_width = slide.find('ul').width();
    slide_max_margin = slide_ul_width;


    function do_animate(object, value) {
        object.stop().animate({
            marginLeft: -value + 'px'
        }, config.speed, function () {
            click = true;
        });
    } // do_animate()


    $('.slide-next').click(function (e) {
        e.preventDefault();
        if (click) {
            click = false;

            slide_margin = parseInt(slide.find('ul').css('margin-left'));
            if (slide_margin < 0) slide_margin = slide_margin * (-1);
            slide_new_margin = slide_margin + slide_li_width;

            if (slide_new_margin < slide_max_margin && slide_new_margin != slide_max_margin) {
                do_animate(slide.find('ul'), slide_new_margin);
            } else {
                do_animate(slide.find('ul'), 0)
            }
        }
    });


    $('.slide-prev').click(function (e) {
        e.preventDefault();
        if (click) {
            click = false;

            slide_margin = parseInt(slide.find('ul').css('margin-left'));
            if (slide_margin < 0) slide_margin = slide_margin * (-1);
            slide_new_margin = slide_margin - slide_li_width;

            if (slide_margin == 0) {
                value = slide_max_margin - slide_li_width;
                do_animate(slide.find('ul'), (slide_max_margin - slide_li_width));
            } else {
                do_animate(slide.find('ul'), slide_new_margin)
            }
        }
    });
} // slider_simple()