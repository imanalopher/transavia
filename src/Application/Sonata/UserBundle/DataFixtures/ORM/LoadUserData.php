<?php

namespace Application\Sonata\UserBundle\DataFixture\ORM;


use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData implements FixtureInterface
{

    function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setUsername("admin");
        $userAdmin->setEmail('admin');
        $userAdmin->setPlainPassword('password');
        $userAdmin->setSuperAdmin(true);
        $userAdmin->setEnabled(true);

        $manager->persist($userAdmin);
        $manager->flush();
    }
}