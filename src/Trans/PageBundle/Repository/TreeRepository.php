<?php

namespace Trans\PageBundle\Repository;


use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Trans\PageBundle\Entity\Page;

class TreeRepository extends NestedTreeRepository
{
    public function getArticlesByParentIds($treeIds, $quantity = 1, $page = 1, $level = Page::THIRD_LEVEL)
    {
        $quantity = ($page) * $quantity;

        $queryBuilder = $this->getEntityManager()->createQueryBuilder('p');
        $queryResult = $queryBuilder
            ->select('p')
            ->setMaxResults($quantity)
            ->from('TransPageBundle:Page', 'p')
            ->where('p.lvl = :level')
            ->setParameter('level', $level)
            ->andWhere($queryBuilder->expr()->in('p.parent', $treeIds));

        return $queryResult->getQuery()->getResult();
    }

    public function getArticles($exceptId, $parentId, $quantity = 2)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder('p');
        $queryResult = $queryBuilder
            ->select('p')
            ->setMaxResults($quantity)
            ->from('TransPageBundle:Page', 'p')
            ->where('p.parent = :parent')
            ->setParameter('parent', $parentId)
            ->andWhere($queryBuilder->expr()->notIn('p.id', $exceptId));

        return $queryResult->getQuery()->getResult();
    }
} 