<?php

namespace Trans\PageBundle\Admin;


use Gedmo\Sluggable\Util\Urlizer;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Trans\PageBundle\Entity\Page;

class PageAdmin extends Admin
{
    protected $maxPerPage = 2500;
    protected $maxPageLinks = 2500;

    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'lft'
    );

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $query->andWhere(
            $query->expr()->isNotNull($query->getRootAlias() . '.parent')
        );

        return $query;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('up', 'text', array(
                'template' => 'TransPageBundle:PageTreeSort:field_tree_up.html.twig',
                'label' => ' '
            ))
            ->add('down', 'text', array(
                'template' => 'TransPageBundle:PageTreeSort:field_tree_down.html.twig',
                'label' => ' '
            ))
            ->add('id', null, array(
                'sortable' => false
            ))
            ->add('parent',null,array(
                'label' => 'Родитель'
            ))
            ->addIdentifier('leveled_title', null, array(
                'sortable' => false,
                'label' => 'Название страницы'
            ))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()
                ), 'label' => 'Действия'
            ));
    }

    protected function configureFormFields(FormMapper $form)
    {
        $subject = $this->getSubject();
        $id = $subject->getId();

        $form
            ->with('Общие')
            ->add('parent', null, array(
                'label' => 'Родитель',
                'required' => true,
                'query_builder' => function ($er) use ($id) {
                        $queryBuilder = $er->createQueryBuilder('p');
                        if ($id) {
                            $queryBuilder
                                ->where('p.id <> :id')
                                ->setParameter('id', $id);
                        }
                        $queryBuilder
                            ->orderBy('p.lft', 'ASC');
                        return $queryBuilder;
                    }
            ))
            ->add('translations', 'a2lix_translations', array(
                    'fields' => array(
                        'title' => array(
                            'label' => 'Название в меню',
                            'attr' => array(
                                'style' => 'width: 100%'
                            )
                        ),
                        'announcement' => array(
                            'label' => 'Название в тексте',
                            'attr' => array(
                                'class' => 'span10',
                                'rows' => 5,
                                'style' => 'width: 100%'
                            )
                        ),
                        'content' => array(
                            'field_type' => 'ckeditor',
                            'label' => 'Content',
                            'attr' => array(
                                'class' => 'span10',
                                'rows' => 20
                            ),
                            'config' => array('allowedContent' => true)
                        )
                    )
                )
            )
            ->with('Услуга')
            ->add('partial',null,array('label'=>'Связанный блок(опционально)','required'=>false))
            ->add('media', 'sonata_media_type', array(
                'provider' => 'sonata.media.provider.image',
                'context' => 'page',
                'required' => false,

            ))
            ->add('icon', 'sonata_media_type',array('label'=>'Иконка',
                'provider' => 'sonata.media.provider.image',
                'context' => 'icon',
                'required' => false))
            ->end();
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter
            ->add('title');
    }

    /** @var Page $object */
    public function postPersist($object)
    {
        $em = $this->modelManager->getEntityManager($object);
        $repo = $em->getRepository("TransPageBundle:Page");
        $repo->verify();
        $repo->recover();
        $em->flush();
    }

    public function postUpdate($object)
    {
        $this->postPersist($object);
    }
}