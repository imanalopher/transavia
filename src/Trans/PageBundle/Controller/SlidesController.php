<?php

namespace Trans\PageBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Trans\PageBundle\Entity\Page;

class SlidesController extends Controller
{
    public function slidesAction()
    {
        $slider = $this->getDoctrine()->getRepository('TransPageBundle:Page')->findOneBy(array(
            'slug' => Page::SERVICES_SLUG
        ));

        return $this->render('@TransPage/Slider/slider.html.twig', array(
            'slides' => $slider->getChildren()
        ));
    }

    public function miniSliderAction(Page $page)
    {
        $left = null;
        $right = null;
        if ($page->getLft())
            $left = $this->getDoctrine()->getRepository('TransPageBundle:Page')->find($page->getLft());
        if ($left == null || $left->getParent()== null || $left->getParent()!=$page->getParent())
            $left = null;

        if ($page->getRgt())
            $right = $this->getDoctrine()->getRepository('TransPageBundle:Page')->find($page->getRgt());
        if ($right == null || $right->getParent()== null || $right->getParent()!=$page->getParent())
            $right = null;

        return $this->render('@TransPage/Slider/mini_slider.html.twig', array(
            'left' => $left,
            'right'=>$right
        ));
    }
} 