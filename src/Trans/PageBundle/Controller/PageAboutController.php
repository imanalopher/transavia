<?php

namespace Trans\PageBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Trans\PageBundle\Entity\Page;

class PageAboutController extends Controller
{
    public function aboutAction()
    {
        $aboutPage = $this->getDoctrine()->getRepository('TransPageBundle:Page')->findOneBy(array(
            'slug' => Page::ABOUT_SLUG
        ));

        $children = $aboutPage->getChildren();

        return $this->render('@TransPage/PageAbout/about.html.twig', array(
            'page' => $aboutPage,
            'children' => $children
        ));
    }

    public function subPageAction(Request $request)
    {
        $slug = $request->get('slug');

        $page = $this->getDoctrine()->getRepository('TransPageBundle:Page')->findOneBy(array(
            'slug' => $slug
        ));

        if (!$page) {
            throw new EntityNotFoundException();
        }

        return $this->render('@TransPage/PageService/service.html.twig', array(
            'page' => $page
        ));
    }

    public function  lufthansaAction()
    {

        return $this->render('@TransPage/PageAbout/page.html.twig');

    }
} 