<?php
/**
 * Created by PhpStorm.
 * User: isabek
 * Date: 22.05.14
 * Time: 13:52
 */

namespace Trans\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageTreeSortController extends Controller
{
    public function upAction($page_id)
    {
        $repository = $this->getDoctrine()->getRepository('TransPageBundle:Page');
        $page = $repository->find($page_id);

        if ($page->getParent()) {
            $repository->moveUp($page);
        }

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

    public function downAction($page_id)
    {

        $repository = $this->getDoctrine()->getRepository('TransPageBundle:Page');
        $page = $repository->find($page_id);

        if ($page->getParent()) {
            $repository->moveDown($page);
        }

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }
} 