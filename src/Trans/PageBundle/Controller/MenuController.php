<?php
namespace Trans\PageBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Trans\PageBundle\Entity\Page;

class MenuController extends Controller
{
    public function aboutMenuAction()
    {
        $page = $this->getDoctrine()->getRepository('TransPageBundle:Page')->findOneBy(array(
            'slug' => Page::ABOUT_SLUG
        ));

        return $this->render('@TransPage/SubMenu/about.html.twig', array(
            'children' => $page->getChildren()
        ));
    }

    public function serviceMenuAction()
    {
        $page = $this->getDoctrine()->getRepository('TransPageBundle:Page')->findOneBy(array(
            'slug' => Page::SERVICES_SLUG
        ));


        return $this->render('@TransPage/SubMenu/services.html.twig', array(
            'children' => $page->getChildren()
        ));
    }

    public function breadcrumbAction(Request $request)
    {
        $route = $request->get('route');
        $params = $request->get('params');

        $menus = array();

        if ($route != 'trans_main_homepage') {
            $menus[] =
                array(
                    'route' => 'trans_main_homepage',
                    'title' => 'Home',
                    'params' => array()
                );
        }

        if ($route == 'trans_page_useful_info') {
            $menus[] = array(
                'route' => $route,
                'params' => $params,
                'title' => 'Useful information'
            );
        } elseif ($route == 'trans_page_about') {
            $menus[] = array(
                'route' => $route,
                'params' => $params,
                'title' => 'About company'
            );
        } elseif ($route == 'trans_page_about_sub_page') {
            $menus[] = array(
                'route' => 'trans_page_about',
                'params' => array(),
                'title' => 'About company',
                'children' => true
            );

            $page = $this->getDoctrine()->getRepository('TransPageBundle:Page')->findOneBy(array(
                'slug' => $params['slug']
            ));

            $menus[] = array(
                'route' => $route,
                'params' => $params,
                'title' => $page->getTitle()
            );
        } elseif ($route == 'trans_page_service_sub_page') {
            $menus[] = array(
                'route' => 'trans_page_services',
                'params' => array(),
                'title' => 'Services',
                'children' => true
            );

            $page = $this->getDoctrine()->getRepository('TransPageBundle:Page')->findOneBy(array(
                'slug' => $params['slug']
            ));

            $menus[] = array(
                'route' => $route,
                'params' => $params,
                'title' => $page->getTitle()
            );

        } elseif ($route == 'trans_page_services') {
            $menus[] = array(
                'route' => $route,
                'params' => $params,
                'title' => 'Services'
            );
        } elseif ($route == 'trans_news_list') {
            $menus[] = array(
                'route' => $route,
                'params' => $params,
                'title' => 'Company news'
            );
        } elseif ($route == 'trans_partner_homepage') {
            $menus[] = array(
                'route' => $route,
                'params' => $params,
                'title' => 'Partners'
            );
        } elseif ($route == 'trans_partner_schedule') {
            $menus[] = array(
                'route' => $route,
                'params' => $params,
                'title' => 'Flight schedule'
            );
        } elseif ($route == 'trans_service_contacts') {
            $menus[] = array(
                'route' => $route,
                'params' => $params,
                'title' => 'Contacts'
            );
        }

        return $this->render('@TransPage/Breadcrumb/breadcrumb.html.twig', array(
            'menus' => $menus
        ));
    }
}