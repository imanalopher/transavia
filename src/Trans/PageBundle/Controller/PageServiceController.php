<?php

namespace Trans\PageBundle\Controller;


use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Trans\PageBundle\Entity\Page;

class PageServiceController extends Controller
{
    public function servicesAction()
    {
        $page = $this->getDoctrine()->getRepository('TransPageBundle:Page')->findOneBy(array(
            'slug' => Page::SERVICES_SLUG
        ));

        if (!$page) {
            throw new EntityNotFoundException();
        }

        return $this->render('@TransPage/PageService/services.html.twig', array(
            'page' => $page
        ));
    }

    public function serviceChildrenAction(Request $request)
    {
        $slug = $request->get('slug');

        $page = $this->getDoctrine()->getRepository('TransPageBundle:Page')->findOneBy(array(
            'slug' => $slug
        ));

        if (!$page) {
            throw new EntityNotFoundException();
        }

        if (in_array($slug, array(Page::SERVICE_TRAIN_PAGE_SLUG, Page::SERVICE_AVIA_PAGE_SLUG))) {
            return $this->render(sprintf('@TransPage/PageService/%s.html.twig', $slug), array(
                'page' => $page
            ));
        }

        return $this->render('@TransPage/PageService/service.html.twig', array(
            'page' => $page
        ));
    }

} 