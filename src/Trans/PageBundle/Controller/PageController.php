<?php

namespace Trans\PageBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Trans\PageBundle\Entity\Page;

class PageController extends Controller
{
    public function usefulInfoAction()
    {
        $quantity = $this->get('service_container')->getParameter('articles_limit');

        $treeRepository = $this->getDoctrine()->getRepository('TransPageBundle:Page');

        $rubrics = $this->getRubrics();

        $ids = array(0);
        foreach ($rubrics as $rubric) {
            $ids[] = $rubric->getId();
        }

        $articles = $treeRepository->getArticlesByParentIds($ids, $quantity);

        return $this->render('@TransPage/Page/useful_info_list.html.twig', array(
            'rubrics' => $rubrics,
            'articles' => $articles
        ));
    }

    public function articleAction(Request $request)
    {

        $slug = $request->get('slug');

        /** @var Page $article */
        $treeRepository = $this->getDoctrine()->getRepository('TransPageBundle:Page');

        $article = $treeRepository->findOneBy(array(
            'slug' => $slug
        ));

        if (!$article) {
            throw new EntityNotFoundException();
        }

        $articles = $treeRepository->getArticles($article->getId(), $article->getParent()->getId());

        return $this->render('@TransPage/PageArticle/article.html.twig', array(
            'article' => $article,
            'articles' => $articles
        ));
    }

    public function loadArticlesAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new HttpException('Request must be a ajax request');
        }

        $page = $request->get('page', 1);
        $rubricId = $request->get('rubric', 0);
        $quantity = $this->get('service_container')->getParameter('articles_limit');

        $treeRepository = $this->getDoctrine()->getRepository('TransPageBundle:Page');

        $rubrics = $this->getRubrics();

        $ids = array(0);
        foreach ($rubrics as $rubric) {
            $ids[] = $rubric->getId();
        }

        $ids = in_array($rubricId, $ids) && $rubricId != 0 ? $rubricId : $ids;
        $articles = $treeRepository->getArticlesByParentIds($ids, $quantity, $page);


        return new JsonResponse(array(
            'html' => $this->renderView('@TransPage/Page/_announcements.html.twig', array(
                    'articles' => $articles
                )),
            'page' => ($page * $quantity) <= count($articles) ? $page + 1 : $page
        ));
    }

    protected function getRubrics()
    {
        $treeRepository = $this->getDoctrine()->getRepository('TransPageBundle:Page');

        $usefulInfoPage = $treeRepository->findOneBy(array(
            'slug' => Page::USEFUL_INFO_SLUG
        ));

        $rubrics = $treeRepository->findBy(array(
            'lvl' => Page::SECOND_LEVEL,
            'parent' => $usefulInfoPage
        ));

        return $rubrics;
    }
} 