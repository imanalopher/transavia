<?php
/**
 * Created by PhpStorm.
 * User: isabek
 * Date: 01.06.14
 * Time: 15:37
 */

namespace Trans\PageBundle\Twig;


class ToIntExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(new \Twig_SimpleFunction('toint', function ($value) {
            return intval($value);
        }));
    }

    public function getName()
    {
        return 'toint';
    }
}