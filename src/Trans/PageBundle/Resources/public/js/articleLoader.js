var article = {
    options: {
        url: null
    },
    init: function (options) {
        $.extend(article.options, options);
        article.initEvents();
    },
    initEvents: function () {
        $('#load-more-articles').on('click', function () {
            article.load(article.getData());
        });

        $('#useful-info-rubrics').on('change', function () {
            article.load(article.getData());
        });
    },
    getData: function () {
        return {
            'page': $('#page-id').val(),
            'rubric': $('#useful-info-rubrics').val()
        };
    },
    setPageId: function (page) {
        $('#page-id').val(page);
    },
    load: function (data) {
        $.get(article.options.url, data, function (result) {
            article.setPageId(result.page);
            $('.b-four-col').html(result.html);
        });
    }
};
