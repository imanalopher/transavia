<?php
namespace Trans\PageBundle\Listener;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Gedmo\Sluggable\Util\Urlizer;
use Trans\PageBundle\Entity\Page;
use Trans\PageBundle\Entity\PageTranslation;


class PageListener
{

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Page && !$entity->getSlug()) {
            /** @var ArrayCollection $arrayCollection */
            $arrayCollection = $entity->getTranslations();

            if ($arrayCollection->isEmpty()) {
                throw new \Exception('Page has not PageTranslation child!');
            }

            /** @var PageTranslation $pageTranslation */
            $pageTranslation = $arrayCollection->first();

            $slug = Urlizer::transliterate($pageTranslation->getTitle());
            $entity->setSlug($slug);
        }
    }
} 