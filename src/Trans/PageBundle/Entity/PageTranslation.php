<?php

namespace Trans\PageBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;

/**
 * @ORM\Table("page_page_translation")
 * @ORM\Entity
 */
class PageTranslation extends AbstractTranslation
{

    /**
     * @Prezent\Translatable(targetEntity="Trans\PageBundle\Entity\Page")
     */
    protected $translatable;


    /**
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(name="content", type="text")
     */
    private $content;


    /**
     * @ORM\Column(name="announcement", type="text")
     */
    protected $announcement;


    /**
     * Set title
     *
     * @param string $title
     * @return PageTranslation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return PageTranslation
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public static function getTranslationEntityClass()
    {
        return 'PageTranslation';
    }

    /**
     * @return mixed
     */
    public function getAnnouncement()
    {
        return $this->announcement;
    }

    public function setAnnouncement($announcement)
    {
        $this->announcement = $announcement;

        return $this;
    }
}
