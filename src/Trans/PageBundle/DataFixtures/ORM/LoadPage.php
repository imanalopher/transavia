<?php
/**
 * Created by PhpStorm.
 * User: isabek
 * Date: 22.05.14
 * Time: 12:52
 */

namespace Trans\PageBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\PageBundle\Entity\Page;

class LoadPage implements FixtureInterface, OrderedFixtureInterface
{


    function load(ObjectManager $manager)
    {
        $rootPage = new Page();

        $rootPage->translate('ru')
            ->setTitle('--Корневой элемент--')
            ->setContent('--Корневой элемент--')
            ->setAnnouncement('--Корневой элемент--');

        $rootPage->translate('en')
            ->setTitle('--Root Menu--')
            ->setContent('--Root Menu--')
            ->setAnnouncement('--Root Menu--');

        $rootPage->setSlug('root');
        $rootPage->setLft(0);
        $rootPage->setRgt(0);
        $rootPage->setLvl(0);
        $rootPage->setRoot(0);

        $manager->persist($rootPage);

        $servicesPage = new Page();
        $servicesPage->setSlug(Page::SERVICES_SLUG);

        $servicesPage->translate('ru')
            ->setTitle('услуги')
            ->setContent("<ol>
					<li>
						Бронирование и продажа авиабилетов на все авиакомпании
					</li>
					<li>
						Продажа железнодорожных билетов по Казахстану, России и странам СНГ
					</li>
					<li>
						Бронирование гостиниц в Казахстане и по всему миру;
					</li>
					<li>
						VIP-обслуживание в аэропортах (VIP-залы, ускоренное прохождение таможенного и паспортного контроля, встреча и проводы в аэропорту)
					</li>
					<li>
						организация трансферов и аренды машин, наземного обслуживания;
					</li>
					<li>
						Паспортно-визовые услуги
					</li>
					<li>
						Организация путешествий, деловой туризм, оздоровительно-курортное лечение
					</li>
					<li>
						Медицинское сопровождение на борту самолета
					</li>
					<li>
						Медицинское страхование, выезжающих за рубеж;
					</li>
					<li>
						Автострахование гражданской ответственности.
					</li>
					<li>
						Организация грузовых перевозок
					</li>
					<li>
						Услуги деловой авиации, аренда бизнес джетов;
					</li>
					<li>
						Услуги по проведению конференций, семинаров, поощрительных поездок и торжеств;
					</li>
					<li>
						Доставка почты и корреспонденции
					</li>
					<li>
						Бесплатная доставка билетов в офисы клиентов
					</li>
					<li>
						Возможность открытия имплант-офиса
					</li>
				</ol>
				<p>
					Вы всегда можете приобрести авиабилеты в офисах нашего агентства или купить наиболее дешевые авиабилеты онлайн на www.transavia-travel.kz
				</p>")
            ->setAnnouncement('«Трансавиа» является многопрофильным агентством, предоставляющим своим клиентам широкий выбор услуг:');

        $servicesPage->translate('en')
            ->setTitle('services')
            ->setContent('services')
            ->setAnnouncement('services');

        $servicesPage->setParent($rootPage);
        $manager->persist($servicesPage);

        $aboutPage = new Page();
        $aboutPage->setSlug(Page::ABOUT_SLUG);

        $aboutPage->translate('ru')
            ->setTitle('о компании')
            ->setContent('<p>
					Компания основана в 2000г., многолетний опыт и высокий уровень профессионализма сотрудников позволяет оптимизировать трэвел-бюджет клиентов,сократить расходы и экономить время на организацию командировок и поездок, получив при этом индивидуальный подход к каждому сотруднику клиента и высококлассный сервис. С начала основания компании нашей главной целью было и остается по сей день:качественное обслуживание, максимальная поддержка и детальная консультация клиентов по всем возникающим вопросам. Миссия компании -Выстроить надежные партнерские отношения с теми, кто ценит время и качество обслуживания, предоставляя стабильный сервис на высшем уровне.
				</p>
				<p>
					На сегодняшний день наша компания:
				</p>
				<p>
				<ul>
					<li>
						Имеет сертификацию IATA (Международная Ассоциация Авиаперевозчиков)
					</li>
					<li>
						Является членом Казахстанской Туристской Ассоциации (KTA),
					</li>
					<li>
						Прямой агент по продаже авиабилетов российских авиакомпаний в Казахстане, аккредитовано в транспортной клиринговой палате (ТКП),
					</li>
					<li>
						Сертифицирована комитетом гражданской авиации министерства транспорта и коммуникаций Республики Казахстан
					</li>
				</ul>
				<p>
					"Трансавиа" является Генеральным Агентством и выполняет представительские функции многих авиакомпаний в городах и странах согласно перечню ниже:
				</p>
				<p>
					"Siberia Airlines" (S7) - Алматы, Представительство авиакомпании;
				</p>
				<p>
					"Transaero" (UN) - Актау, Атырау, Уральск, Шымкент, Кокшетау, Актобе, Представительство а/к;
				</p>
				<p>
					"МАУ" - (PS) (Ukraine International Airlines):
				</p>
				<p>
					Генеральное агентство: Республика Казахстан, Республика Беларусь (г.Минск);<br>
					Генеральное агентство и представительство: Киргизская Республика (г.Бишкек);<br>
					 "Uzbekiston Havo Yollari" (HY) - Генеральное агентство в РК;<br>
					 "Hainan Airlines" (HU) - Генеральное агентство в РК; <br>
					 "Bek Air" (Z9) - Генеральное агентство в РК; <br>
					 "Orenair" (R2) - Генеральное агентство и представительство в РК;<br>
					Мы работаем во благо клиента и обеспечиваем взаимный рост, благосостояние и доверие каждой из сторон.
				<p>')
            ->setAnnouncement('о компании');

        $aboutPage->translate('en')
            ->setTitle('about')
            ->setContent('about')
            ->setAnnouncement('about');

        $aboutPage->setParent($rootPage);
        $manager->persist($aboutPage);

        $usefulInfo = new Page();
        $usefulInfo->setSlug(Page::USEFUL_INFO_SLUG);
        $usefulInfo->translate('ru')
            ->setTitle('полезная информация')
            ->setContent('полезная информация')
            ->setAnnouncement('полезная информация');

        $usefulInfo->translate('en')
            ->setTitle('useful information')
            ->setContent('useful information')
            ->setAnnouncement('useful information');

        $usefulInfo->setParent($rootPage);
        $manager->persist($usefulInfo);


        $slidePages = array(
            array(
                'title' => 'Авиабилеты',
                'slug' => 'slide-avia',
                "announcement" => "найти, купить билет в офисах или онлайн на нашем сайте."
            ),
            array(
                'title' => 'Трансфер',
                'slug' => 'slide-transfer',
                "announcement" => "когда нет авиа или ж/д сообщения до конечного требуемого пункта"
            ),
            array(
                'title' => 'Ж/Д касса',
                'slug' => 'slide-rails',
                "announcement" => "найти, купить билет в офисах или онлайн на нашем сайте."
            ),
            array(
                'title' => 'Бронь отелей',
                'slug' => 'slide-hotels',
                "announcement" => "наши специалисты помогут определиться с местом"
            ),
            array(
                'title' => 'Грузоперевозки',
                'slug' => 'slide-trucking',
                "announcement" => "продажа грузовых перевозок ведущих авиакомпаний"
            ),
            array(
                'title' => 'Тур услуги',
                'slug' => 'slide-tour',
                "announcement" => "найти, купить билет в офисах или онлайн на нашем сайте."

            ),
            array(
                'title' => 'MICE услуги',
                'slug' => 'slide-mice',
                "announcement" => "концепция комплексного корпоративного обслуживания"
            ),
            array(
                'title' => 'Визовая поддержка',
                'slug' => 'slide-visa',
                "announcement" => "оформление деловых и туристических виз для иностранных граждан"
            ),
            array(
                'title' => 'Чартерные рейсы',
                'slug' => 'slide-charter',
                "announcement" => "программа организации и выполнения чартерных рейсов"
            ),
            array(
                'title' => 'VIP обслуживание',
                'slug' => 'slide-vip',
                "announcement" => "сэкономит Ваше время при прилете, вылете или транзите."
            ),
            array(
                'title' => 'Мед. соправождение',
                'slug' => 'slide-medical',
                "announcement" => "медецинское соправождение на борту самолета"
            ),
            array(
                'title' => 'Мед. страхование',
                'slug' => 'slide-med-insurance',
                "announcement" => "мед. страхование, выезжающих за рубеж граждан"
            ),
            array(
                'title' => 'Автострахование',
                'slug' => 'slide-auto-insurance',
                "announcement" => "автострахование гражданской ответственности"
            ),
            array(
                'title' => 'Доставка почты',
                'slug' => 'slide-mail-delivery',
                "announcement" => "Доставка почты и корреспонденции в офис клиента"
            ),
            array(
                'title' => 'Доставка билетов',
                'slug' => 'slide-ticket-delivery',
                "announcement" => "бесплатная доставка билетов в офисы клиентов"
            ),
            array(
                'title' => 'Деловая авиация',
                'slug' => 'slide-business-avia',
                "announcement" => "услуги деловой авиации, аренда бизнес джетов"
            ),
            array(
                'title' => 'Конференции',
                'slug' => 'slide-conference',
                "announcement" => "услуги по проведению конференций, семинаров, торжеств..."
            ),
            array(
                'title' => 'Имплант-офис',
                'slug' => 'slide-implant',
                "announcement" => "возможность открытия имплант-офиса"
            ),
        );

        $sliderPage = new Page();
        $sliderPage->translate('ru')
            ->setTitle('Слайдер')
            ->setContent('Слайдер')
            ->setAnnouncement('Слайдер');

        $manager->persist($sliderPage);

        foreach ($slidePages as $slidePage) {
            $page = new Page();
            $page->translate('ru')
                ->setTitle($slidePage['title'])
                ->setContent(sprintf('<p>%s</p>', $slidePage['announcement']))
                ->setAnnouncement($slidePage['announcement']);

            $page->setSlug($slidePage['slug']);
            $page->setParent($sliderPage);
            $manager->persist($page);
        }


        $serviceTrain = new Page();
        $serviceTrain->setSlug(Page::SERVICE_TRAIN_PAGE_SLUG);
        $serviceTrain->translate('ru')
            ->setTitle('ж/д билеты')
            ->setContent('
            <p>Конвесия покупателя концентрирует CTR, осознав маркетинг как часть производства. Медийная реклама требовальна к креативу. Практика однозначно показывает, что рекламный макет программирует общественный нестандартный подход, опираясь на опыт западных коллег.</p>
            <ul>
                <li class="b-rails-offices__item">
                    <div class="b-office-contacts">
                        <ul>
                            <li class="b-office-contacts__item b-office__address">
										<span class="">
											Алматы, Керемет, 5 - ул. Сейфуллина ,
											уг. Никитина (ниже Тимирязева, заезд
											с Сейфуллина)<br>
											часы работы с пн. по пт.<br>
											с 09.00 – 19.00
										</span>
                            </li>
                            <li class="b-office-contacts__item b-office__phone">
                                <span class="tel-code">+7 (727)</span> 315 55 30
                            </li>
                            <li class="b-office-contacts__item b-office__mail">
                                <a href="mailto:info@transavia-travel.kz">info@transavia-travel.kz</a>
                            </li>
                        </ul>
                    </div>
                    <!-- b-rails-office-contacts -->
                </li>


                <li class="b-rails-offices__item">
                    <div class="b-office-contacts">
                        <ul>
                            <li class="b-office-contacts__item b-office__address">
										<span class="">
											Алматы, пр. Достык , 85, <br><br>
											часы работы ежедневно с<br>
											09.00 – 19.00
										</span>
                            </li>
                            <li class="b-office-contacts__item b-office__phone">
                                <span class="tel-code">+7 (7172)</span> 390 80 68
                            </li>
                        </ul>
                    </div>
                    <!-- b-rails-office-contacts -->
                </li>


                <li class="b-rails-offices__item">
                    <div class="b-office-contacts">
                        <ul>
                            <li class="b-office-contacts__item b-office__address">
										<span class="">
											Алматы, ул. Желтоксан, 104
											(уг. ул. Айтеке би)<br>
											часы работы ежедневно с<br>
											09.00 – 19.00
										</span>
                            </li>
                            <li class="b-office-contacts__item b-office__phone">
                                <span class="tel-code">+7 (7172)</span> 279 59 59
                            </li>
                        </ul>
                    </div>
                    <!-- b-rails-office-contacts -->
                </li>


                <li class="b-rails-offices__item">
                    <div class="b-office-contacts">
                        <ul>
                            <li class="b-office-contacts__item b-office__address">
										<span class="">
											Алматы, Керемет, 5 - ул. Сейфуллина ,
											пр. Суюнбая (ниже Раимбека, бизнес
											центр "Мерей")<br><br>
											часы работы ежедневно с<br>
											09.00 – 19.00
										</span>
                            </li>
                            <li class="b-office-contacts__item b-office__phone">
                                <span class="tel-code">+7 (7172)</span> 259 81 41
                            </li>
                        </ul>
                    </div>
                    <!-- b-rails-office-contacts -->
                </li>
            </ul>')
            ->setAnnouncement('Железнодорожная касса');

        $serviceTrain->setParent($servicesPage);
        $manager->persist($serviceTrain);


        $serviceCargo = new Page();
        $serviceCargo->setSlug(Page::SERVICE_CARGO_PAGE_SLUG);
        $serviceCargo->translate('ru')
            ->setTitle('грузоперевозки')
            ->setContent('<p>Предлагаем услуги по перевозке груза на рейсах АК"British Airways", "KLM", "Люфтганза", "Турецкие Авиалинии", "Аэрофлот", "Узбекские авиалинии".
				</p>
				<p>
					При отсутствии прямого рейса обеспечиваем транзит через ближайший аэропорт, разрабатываем наиболее короткий маршрут следования Вашего груза. Оформляем перевозку по тарифам авиакомпаний с предоставлением экземпляра грузовой накладной отправителю. Бронируем груз по всему маршруту, что позволяет получать актуальное расписание рейсов авиакомпаний с учетом изменений типов ВС. Предоставляем информацию о доступных и планируемых грузовых емкостях в режиме реального времени,  отслеживаем движение груза до момента получения в пункте назначения. Оказываем консультативные услуги по таможенным формальностям в аэропорту отправления. Перевозим тяжеловесный негабаритный груз, работаем с опасным грузом. Оказываем услуги по отправке почты и мелких грузов "от двери до двери" по маршрутам РК. Отправка «командирской почты» Алматы-Москва рейсами Авиакомпании «Эйр Астана»
				</p>
				<p>
					Продажа указанных перевозчиков ведется в филиалах ТОО "Трансавиа": Астана, Актау, Атырау, Актобе, Уральск, Усть-Каменогорск, Караганда, Костанай, Шымкент.
				</p>
                <ul>
					<li class="b-services-contacts__item b-services__address-all">
						ул. Желтоксан, 104 (уг. ул. Айтеке би)<br>
						часы работы с 09.00 – 19.00<br>
						(вых. суббота, воскресенье)<br>
						<span class="red underline underline-red">Телефоны и адреса всех офисов</span>
					</li>
					<li class="b-services-contacts__item b-services__phone">
						<span class="b-footer__tel-code">+7 (727)</span> 258 33 36
					</li>
					<li class="b-services-contacts__item b-services__mail">
						<a href="mailto:cargo@transavia-travel.kz">cargo@transavia-travel.kz</a>
					</li>
				</ul>
				<ul class="b-services-social last">
					<li class="b-services-social__item facebook">
						<a href="#">www.facebook.com/transavia</a>
					</li>
					<li class="b-services-social__item twitter">
						<a href="#">www.twitter.com/transavia</a>
					</li>
					<li class="b-services-social__item vk">
						<a href="#">www.vk.com/transavia</a>
					</li>
					<li class="b-services-social__item odnoklassniki">
						<a href="#">www.ok.ru/transavia</a>
					</li>
				</ul>')
            ->setAnnouncement('«Трансавиа» является Агентом по продаже грузовых перевозок ведущих зарубежных и казахстанских авиакомпаний.');

        $serviceCargo->setParent($servicesPage);
        $manager->persist($serviceCargo);

        $manager->flush();

    }

    function getOrder()
    {
        return 1;
    }
}