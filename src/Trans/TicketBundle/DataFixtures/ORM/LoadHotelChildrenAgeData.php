<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/26/14
 * Time: 11:23 AM
 */

namespace Trans\TicketBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\TicketBundle\Entity\HotelChildren;
use Trans\TicketBundle\Entity\HotelChildrenAge;

class LoadHotelChildrenData extends AbstractFixture implements OrderedFixtureInterface
{

    function load(ObjectManager $manager)
    {
        $values = array('0-2', '3-5', '6-8');
        foreach ($values as $value) {
            $hotelChildrenAge = new HotelChildrenAge();
            $hotelChildrenAge->setName($value);
            $manager->persist($hotelChildrenAge);
            $manager->flush();
        }
    }

    function getOrder()
    {
        return 1;
    }
}