<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/14/14
 * Time: 1:43 PM
 */

namespace Trans\TicketBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\TicketBundle\Entity\TourProfile;

class LoadTourProfile extends AbstractFixture implements OrderedFixtureInterface
{


    function load(ObjectManager $manager)
    {
        $profiles = array(
            array('ru' => 'SPA', 'en' => 'SPA'),
            array('ru' => 'Круиз', 'en' => 'Cruise'),
            array('ru' => 'Пляжный отдых', 'en' => 'Beach resort'),
            array('ru' => 'Аренда вилл и апартаментов', 'en' => 'Rent of villas or aparments'),
            array('ru' => 'Свадебное путешествие', 'en' => 'Wedding journey'),
            array('ru' => 'Отдых на островах', 'en' => 'Rest on islands'),
            array('ru' => 'Сафари', 'en' => 'Safare'),
            array('ru' => 'Дайвинг', 'en' => 'Diving'),
            array('ru' => 'Элитный отдых', 'en' => 'Selected rest'),
            array('ru' => 'Гольф', 'en' => 'Golf'),
            array('ru' => 'Отдых с детьми', 'en' => 'Rest with kids'),
            array('ru' => 'Экскурсионные туры', 'en' => 'Sightseeing tours'),
            array('ru' => 'Горнолыжные курорты', 'en' => 'Ski resorts'),
            array('ru' => 'Деловой туризм', 'en' => 'Business tourism'),
            array('ru' => 'Роскошные автомобили', 'en' => 'Luxury cars')
        );

        foreach ($profiles as $p) {
            $profile = new TourProfile();
            $profile->translate('ru')->setName($p['ru']);
            $profile->translate('en')->setName($p['en']);
            $manager->persist($profile);
        }
        $manager->flush();
    }


    function getOrder()
    {
        return 1;
    }
}