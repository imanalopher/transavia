<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/19/14
 * Time: 7:59 PM
 */

namespace Trans\TicketBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\TicketBundle\Entity\HotelPlacement;

class LoadHotelPlacementData implements FixtureInterface,OrderedFixtureInterface {

    function load(ObjectManager $manager)
    {
        $translations = array(
            array('ru'=>'SNGL (одноместный)','en'=>'SNGL (single)'),
            array('ru'=>'DBL (двухместный)','en'=>'DBL (double)'),
            array('ru'=>'TRPL (трехместный)','en'=>'DBL (triple)')
        );
        foreach($translations as $em){
            $hotelPlacement = new HotelPlacement();
            $hotelPlacement->translate('ru')->setName($em['ru']);
            $hotelPlacement->translate('en')->setName($em['en']);
            $manager->persist($hotelPlacement);
            $manager->flush();
        }

    }


    function getOrder()
    {
        return 1;
    }
}