<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/19/14
 * Time: 7:59 PM
 */

namespace Trans\TicketBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\TicketBundle\Entity\DocumentType;

class LoadDocumentTypeData implements FixtureInterface,OrderedFixtureInterface {

    function load(ObjectManager $manager)
    {
        $translations = array(array('ru'=>'Удостоверение','en'=>'Identity card'),array('ru'=>'Паспорт','en'=>'Passport'));
        foreach($translations as $em){
            $orderType = new DocumentType();
            $orderType->translate('ru')->setName($em['ru']);
            $orderType->translate('en')->setName($em['en']);
            $manager->persist($orderType);
            $manager->flush();
        }

    }


    function getOrder()
    {
        return 1;
    }
}