<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/14/14
 * Time: 1:54 PM
 */

namespace Trans\TicketBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\TicketBundle\Entity\TourCountry;

class LoadTourCountry extends AbstractFixture implements OrderedFixtureInterface
{


    function load(ObjectManager $manager)
    {
        $countries = array(
            array('ru' => 'Австрия', 'en' => 'Austria'),
            array('ru' => 'Германия', 'en' => 'Germany'),
            array('ru' => 'Англия', 'en' => 'England'),
            array('ru' => 'Египет', 'en' => 'Egypt'),
            array('ru' => 'Израиль', 'en' => 'Israil'),
            array('ru' => 'Иордания', 'en' => 'Jordan'),
            array('ru' => 'Индонезия', 'en' => 'Indonesia'),
            array('ru' => 'Испания', 'en' => 'Spain'),
            array('ru' => 'Италия', 'en' => 'Italy'),
            array('ru' => 'Китай', 'en' => 'China'),
            array('ru' => 'Корея', 'en' => 'Korea'),
            array('ru' => 'Куба', 'en' => 'Cuba'),
            array('ru' => 'Маврикия', 'en' => 'Mauritius'),
            array('ru' => 'Малайзия', 'en' => 'Malaysia'),
            array('ru' => 'Мальдивы', 'en' => 'Maldives'),
            array('ru' => 'ОАЭ', 'en' => 'UAE'),
            array('ru' => 'Сейшелы', 'en' => 'Seychelles'),
            array('ru' => 'Тайланд', 'en' => 'Thailand'),
            array('ru' => 'Турция', 'en' => 'Turkey'),
            array('ru' => 'Франция', 'en' => 'France'),
            array('ru' => 'Чехия', 'en' => 'Czech Republic'),
            array('ru' => 'Шри Ланка', 'en' => 'Sri Lanka'),
        );

        foreach ($countries as $country) {
            $tourCountry = new TourCountry();
            $tourCountry->translate('ru')->setName($country['ru']);
            $tourCountry->translate('en')->setName($country['en']);
            $manager->persist($tourCountry);
        }
        $manager->flush();
    }


    function getOrder()
    {
        return 1;
    }
}