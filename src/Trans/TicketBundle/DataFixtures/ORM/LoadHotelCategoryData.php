<?php

namespace Trans\TicketBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\TicketBundle\Entity\HotelCategory;

class LoadHotelCategoryData implements FixtureInterface,OrderedFixtureInterface {

    function load(ObjectManager $manager)
    {
        $translations =
            array(
                array('ru'=>'5 звездочный','en'=>'5 star'),
                array('ru'=>'4 звездочный','en'=>'4 star'),
                array('ru'=>'3 звездочный','en'=>'3 star'),
                array('ru'=>'2 звездочный','en'=>'2 star'),
                array('ru'=>'1 звездочная','en'=>'1 star'),
                array('ru'=>'Апартамент','en'=>'Apartment')
            );
        foreach($translations as $em){
            $hotelCategory = new HotelCategory();
            $hotelCategory->translate('ru')->setName($em['ru']);
            $hotelCategory->translate('en')->setName($em['en']);
            $manager->persist($hotelCategory);
            $manager->flush();
        }

    }


    function getOrder()
    {
        return 1;
    }
}