<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/14/14
 * Time: 2:15 PM
 */

namespace Trans\TicketBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\TicketBundle\Entity\FoodType;

class LoadTourFood extends AbstractFixture implements OrderedFixtureInterface
{


    function load(ObjectManager $manager)
    {
        $foods = array(
            array('ru' => 'NO (без питания)', 'en' => 'NO (room only)'),
            array('ru' => 'BB (завтрак)', 'en' => 'BB (breakfast)'),
            array('ru' => 'HB (Полупансион)', 'en' => 'HB (half board'),
            array('ru' => 'FB (Полный пансион', 'en' => 'FB (full board'),
            array('ru' => 'ALL (Все включено)', 'en' => 'ALL (All inclusive'),
            array('ru' => 'UAL (Ультра все включено)', 'en' => 'UAL (Ultra all inclusive'),
        );

        foreach ($foods as $food) {
            $foodType = new FoodType();
            $foodType->translate('ru')->setName($food['ru']);
            $foodType->translate('en')->setName($food['en']);
            $manager->persist($foodType);
        }
        $manager->flush();
    }


    function getOrder()
    {
        return 1;
    }
}