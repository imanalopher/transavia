<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/19/14
 * Time: 7:59 PM
 */

namespace Trans\TicketBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\TicketBundle\Entity\Payment;

class LoadPaymentData implements FixtureInterface,OrderedFixtureInterface {

    function load(ObjectManager $manager)
    {
        $translations = array(array('ru'=>'Наличным расчетом','en'=>'Cash'),array('ru'=>'Безналичным расчетом','en'=>'Cashless payment'));
        foreach($translations as $em){
            $payment = new Payment();
            $payment->translate('ru')->setName($em['ru']);
            $payment->translate('en')->setName($em['en']);
            $manager->persist($payment);
            $manager->flush();
        }

    }


    function getOrder()
    {
        return 1;
    }
}