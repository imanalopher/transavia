<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/19/14
 * Time: 7:59 PM
 */

namespace Trans\TicketBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\TicketBundle\Entity\OrderType;

class LoadOrderTypeData implements FixtureInterface,OrderedFixtureInterface {

    function load(ObjectManager $manager)
    {
        $translations = array(array('ru'=>'Бронировать','en'=>'Reserve'),array('ru'=>'Выписать','en'=>'Issue'));
        foreach($translations as $em){
            $orderType = new OrderType();
            $orderType->translate('ru')->setName($em['ru']);
            $orderType->translate('en')->setName($em['en']);
            $manager->persist($orderType);
            $manager->flush();
        }

    }


    function getOrder()
    {
        return 1;
    }
}