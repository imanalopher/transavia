<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/18/14
 * Time: 6:07 PM
 */

namespace Trans\TicketBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;

/**
 * @ORM\Table("order_type_translation")
 * @ORM\Entity
 */
class OrderTypeTranslation extends AbstractTranslation {

    /**
    * @Prezent\Translatable(targetEntity="Trans\TicketBundle\Entity\OrderType")
    */
    protected $translatable;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;


    /**
     * Set name
     *
     * @param string $name
     * @return OrderType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public static function getTranslationEntityClass()
    {
        return 'OrderTypeTranslation';
    }
} 