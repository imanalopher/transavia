<?php

namespace Trans\TicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderFlight
 *
 * @ORM\Table("order_flight")
 * @ORM\Entity
 */
class OrderFlight extends BaseOrderTicket
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime",nullable=true)
     */
    private $birthday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="class_any", type="boolean", nullable=true)
     */
    private $classAny;

    /**
     * @var boolean
     *
     * @ORM\Column(name="class_economy", type="boolean", nullable=true)
     */
    private $classEconomy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="class_business", type="boolean", nullable=true)
     */
    private $classBusiness;

    /**
     * @var boolean
     *
     * @ORM\Column(name="class_first", type="boolean", nullable=true)
     */
    private $classFirst;


    /**
     * @var OrderType
     *
     * @ORM\ManyToOne(targetEntity="OrderType")
     */
    private $orderType;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return OrderFlight
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set classAny
     *
     * @param boolean $classAny
     * @return OrderFlight
     */
    public function setClassAny($classAny)
    {
        $this->classAny = $classAny;

        return $this;
    }

    /**
     * Get classAny
     *
     * @return boolean 
     */
    public function getClassAny()
    {
        return $this->classAny;
    }

    /**
 * Set classEconomy
     *
     * @param boolean $classEconomy
     * @return OrderFlight
     */
    public function setClassEconomy($classEconomy)
    {
        $this->classEconomy = $classEconomy;

        return $this;
    }

    /**
     * Get classEconomy
     *
     * @return boolean 
     */
    public function getClassEconomy()
    {
        return $this->classEconomy;
    }

    /**
     * Set classBusiness
     *
     * @param boolean $classBusiness
     * @return OrderFlight
     */
    public function setClassBusiness($classBusiness)
    {
        $this->classBusiness = $classBusiness;

        return $this;
    }

    /**
     * Get classBusiness
     *
     * @return boolean 
     */
    public function getClassBusiness()
    {
        return $this->classBusiness;
    }

    /**
     * Set classFirst
     *
     * @param boolean $classFirst
     * @return OrderFlight
     */
    public function setClassFirst($classFirst)
    {
        $this->classFirst = $classFirst;

        return $this;
    }

    /**
     * Get classFirst
     *
     * @return boolean 
     */
    public function getClassFirst()
    {
        return $this->classFirst;
    }

    /**
     * @return \Trans\TicketBundle\Entity\OrderType
     */
    public function getOrderType()
    {
        return $this->orderType;
    }

    /**
     * @param \Trans\TicketBundle\Entity\OrderType $orderType
     */
    public function setOrderType(OrderType $orderType)
    {
        $this->orderType = $orderType;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
