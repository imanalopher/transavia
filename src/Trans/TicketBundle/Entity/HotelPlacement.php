<?php

namespace Trans\TicketBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslatable;

/**
 * HotelPlacement
 *
 * @ORM\Table("hotel_placement")
 * @ORM\Entity
 */
class HotelPlacement extends AbstractTranslatable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Prezent\Translations(targetEntity="Trans\TicketBundle\Entity\HotelPlacementTranslation")
     */
    protected $translations;

    /**
     * @Prezent\CurrentLocale
     */
    private $currentLocale;

    private $currentTranslation;


    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function translate($locale = null)
    {
        if (null === $locale) {
            $locale = $this->currentLocale;
        }

        if (!$locale) {
            $locale = "ru";
        }

        if ($this->currentTranslation && $this->currentTranslation->getLocale() === $locale) {
            return $this->currentTranslation;
        }

        if (!$translation = $this->translations->get($locale)) {
            $translation = new HotelPlacementTranslation();
            $translation->setLocale($locale);
            $this->addTranslation($translation);
        }

        $this->currentTranslation = $translation;
        return $translation;
    }

    public function getName()
    {

        return $this->translate()->getName();
    }

    public function setName($name)
    {
        $this->translate()->setName($name);
        return $this;
    }

    public function __toString()
    {
        return $this->getName() != null?$this->getName():$this->translate('ru')->getName();
    }

    public static function getTranslationEntityClass()
    {
        return 'HotelPlacementTranslation';
    }
}
