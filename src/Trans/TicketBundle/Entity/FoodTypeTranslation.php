<?php

namespace Trans\TicketBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;

/**
 * @ORM\Table("tour_food_type_translation")
 * @ORM\Entity
 */
class FoodTypeTranslation extends AbstractTranslation
{

    /**
     * @Prezent\Translatable(targetEntity="Trans\TicketBundle\Entity\FoodType")
     */
    protected $translatable;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;


    /**
     * Set name
     *
     * @param string $name
     * @return FoodType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public static function getTranslationEntityClass()
    {
        return 'FoodTypeTranslation';
    }
} 