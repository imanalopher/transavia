<?php

namespace Trans\TicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseOrderTicket
 */
class BaseOrderTicket extends BaseOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="call_time", type="string", length=100)
     */
    protected $callTime;

    /**
     * @var string
     *
     * @ORM\Column(name="departure_point", type="string", length=255)
     */
    protected $departurePoint;

    /**
     * @var string
     *
     * @ORM\Column(name="destination_point", type="string", length=255)
     */
    protected $destinationPoint;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=100, nullable=true)
     */
    protected $company;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cheapest", type="boolean", nullable=true)
     */
    protected $cheapest;

    /**
     * @var boolean
     *
     * @ORM\Column(name="round_trip", type="boolean",nullable=true)
     */
    protected $roundTrip;

    /**
     * @var integer
     *
     * @ORM\Column(name="pass_infant", type="boolean", nullable=true)
     */
    protected $passInfant;

    /**
     * @var integer
     *
     * @ORM\Column(name="pass_children", type="boolean", nullable=true)
     */
    protected $passChildren;

    /**
     * @var integer
     *
     * @ORM\Column(name="pass_young", type="boolean", nullable=true)
     */
    protected $passYoung;

    /**
     * @var integer
     *
     * @ORM\Column(name="pass_adult", type="boolean", nullable=true)
     */
    protected $passAdult;

    /**
     * @var integer
     *
     * @ORM\Column(name="pass_retired", type="boolean", nullable=true)
     */
    protected $passRetired;


    /**
     * @var Payment
     *
     * @ORM\ManyToOne(targetEntity="Trans\TicketBundle\Entity\Payment")
     */
    protected $payment;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set callTime
     *
     * @param string $callTime
     * @return BaseOrderTicket
     */
    public function setCallTime($callTime)
    {
        $this->callTime = $callTime;

        return $this;
    }

    /**
     * Get callTime
     *
     * @return string
     */
    public function getCallTime()
    {
        return $this->callTime;
    }

    /**
     * Set departurePoint
     *
     * @param string $departurePoint
     * @return BaseOrderTicket
     */
    public function setDeparturePoint($departurePoint)
    {
        $this->departurePoint = $departurePoint;

        return $this;
    }

    /**
     * Get departurePoint
     *
     * @return string
     */
    public function getDeparturePoint()
    {
        return $this->departurePoint;
    }

    /**
     * Set destinationPoint
     *
     * @param string $destinationPoint
     * @return BaseOrderTicket
     */
    public function setDestinationPoint($destinationPoint)
    {
        $this->destinationPoint = $destinationPoint;

        return $this;
    }

    /**
     * Get destinationPoint
     *
     * @return string
     */
    public function getDestinationPoint()
    {
        return $this->destinationPoint;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return BaseOrderTicket
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set cheapest
     *
     * @param boolean $cheapest
     * @return BaseOrderTicket
     */
    public function setCheapest($cheapest)
    {
        $this->cheapest = $cheapest;

        return $this;
    }

    /**
     * Get cheapest
     *
     * @return boolean
     */
    public function getCheapest()
    {
        return $this->cheapest;
    }

    /**
     * Set roundTrip
     *
     * @param boolean $roundTrip
     * @return BaseOrderTicket
     */
    public function setRoundTrip($roundTrip)
    {
        $this->roundTrip = $roundTrip;

        return $this;
    }

    /**
     * Get roundTrip
     *
     * @return boolean
     */
    public function getRoundTrip()
    {
        return $this->roundTrip;
    }

    /**
     * Set passInfant
     *
     * @param integer $passInfant
     * @return BaseOrderTicket
     */
    public function setPassInfant($passInfant)
    {
        $this->passInfant = $passInfant;

        return $this;
    }

    /**
     * Get passInfant
     *
     * @return integer
     */
    public function getPassInfant()
    {
        return $this->passInfant;
    }

    /**
     * Set passChildren
     *
     * @param integer $passChildren
     * @return BaseOrderTicket
     */
    public function setPassChildren($passChildren)
    {
        $this->passChildren = $passChildren;

        return $this;
    }

    /**
     * Get passChildren
     *
     * @return integer
     */
    public function getPassChildren()
    {
        return $this->passChildren;
    }

    /**
     * Set passYoung
     *
     * @param integer $passYoung
     * @return BaseOrderTicket
     */
    public function setPassYoung($passYoung)
    {
        $this->passYoung = $passYoung;

        return $this;
    }

    /**
     * Get passYoung
     *
     * @return integer
     */
    public function getPassYoung()
    {
        return $this->passYoung;
    }

    /**
     * Set passAdult
     *
     * @param integer $passAdult
     * @return BaseOrderTicket
     */
    public function setPassAdult($passAdult)
    {
        $this->passAdult = $passAdult;

        return $this;
    }

    /**
     * Get passAdult
     *
     * @return integer
     */
    public function getPassAdult()
    {
        return $this->passAdult;
    }

    /**
     * Set passRetired
     *
     * @param integer $passRetired
     * @return BaseOrderTicket
     */
    public function setPassRetired($passRetired)
    {
        $this->passRetired = $passRetired;

        return $this;
    }

    /**
     * Get passRetired
     *
     * @return integer
     */
    public function getPassRetired()
    {
        return $this->passRetired;
    }

    /**
     * @return \Trans\TicketBundle\Entity\Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param \Trans\TicketBundle\Entity\Payment $payment
     */
    public function setPayment(Payment $payment)
    {
        $this->payment = $payment;
    }
}
