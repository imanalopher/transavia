<?php

namespace Trans\TicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderTour
 *
 * @ORM\Table("order_tour")
 * @ORM\Entity
 */
class OrderTour extends BaseOrder
{

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=100)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="otherProfile", type="string", length=100)
     */
    private $otherProfile;

    /**
     * @var TourProfile
     * @ORM\ManyToMany(targetEntity="Trans\TicketBundle\Entity\TourProfile")
     * @ORM\JoinTable(name="order_tour_profiles")
     */
    private $profiles;

    /**
     * @var integer
     *
     * @ORM\Column(name="adults", type="smallint")
     */
    private $adults;

    /**
     * @var integer
     *
     * @ORM\Column(name="children",type="smallint")
     */
    private $children;

    /**
     * @var TourCountry
     *
     * @ORM\ManyToOne(targetEntity="Trans\TicketBundle\Entity\TourCountry")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * @var HotelCategory
     * @ORM\ManyToOne(targetEntity="Trans\TicketBundle\Entity\HotelCategory")
     * @ORM\JoinColumn(name="hotel_category_id", referencedColumnName="id")
     */
    private $hotelCategory;

    /**
     * @var HotelPlacement
     * @ORM\ManyToOne(targetEntity="Trans\TicketBundle\Entity\HotelPlacement")
     * @ORM\JoinColumn(name="hotel_placement_id", referencedColumnName="id")
     */
    private $hotelPlacement;

    /**
     * @var FoodType
     * @ORM\ManyToOne(targetEntity="Trans\TicketBundle\Entity\FoodType")
     * @ORM\JoinColumn(name="food_type_id", referencedColumnName="id")
     */
    private $foodType;

    /**
     * @var HotelChildrenAge
     * @ORM\ManyToOne(targetEntity="Trans\TicketBundle\Entity\HotelChildrenAge")
     * @ORM\JoinColumn(name="hotel_children_id", referencedColumnName="id")
     */
    private $hotelChildrenAge;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->profiles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set city
     *
     * @param string $city
     * @return OrderTour
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Add profiles
     *
     * @param \Trans\TicketBundle\Entity\TourProfile $profiles
     * @return OrderTour
     */
    public function addProfile(\Trans\TicketBundle\Entity\TourProfile $profiles)
    {
        $this->profiles[] = $profiles;

        return $this;
    }

    /**
     * Remove profiles
     *
     * @param \Trans\TicketBundle\Entity\TourProfile $profiles
     */
    public function removeProfile(\Trans\TicketBundle\Entity\TourProfile $profiles)
    {
        $this->profiles->removeElement($profiles);
    }

    /**
     * Get profiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfiles()
    {
        return $this->profiles;
    }

    /**
     * Set country
     *
     * @param \Trans\TicketBundle\Entity\TourCountry $country
     * @return OrderTour
     */
    public function setCountry(\Trans\TicketBundle\Entity\TourCountry $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Trans\TicketBundle\Entity\TourCountry
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set hotelCategory
     *
     * @param \Trans\TicketBundle\Entity\HotelCategory $hotelCategory
     * @return OrderTour
     */
    public function setHotelCategory(\Trans\TicketBundle\Entity\HotelCategory $hotelCategory = null)
    {
        $this->hotelCategory = $hotelCategory;

        return $this;
    }

    /**
     * Get hotelCategory
     *
     * @return \Trans\TicketBundle\Entity\HotelCategory
     */
    public function getHotelCategory()
    {
        return $this->hotelCategory;
    }

    /**
     * Set hotelPlacement
     *
     * @param \Trans\TicketBundle\Entity\HotelPlacement $hotelPlacement
     * @return OrderTour
     */
    public function setHotelPlacement(\Trans\TicketBundle\Entity\HotelPlacement $hotelPlacement = null)
    {
        $this->hotelPlacement = $hotelPlacement;

        return $this;
    }

    /**
     * Get hotelPlacement
     *
     * @return \Trans\TicketBundle\Entity\HotelPlacement
     */
    public function getHotelPlacement()
    {
        return $this->hotelPlacement;
    }

    /**
     * Set foodType
     *
     * @param \Trans\TicketBundle\Entity\FoodType $foodType
     * @return OrderTour
     */
    public function setFoodType(\Trans\TicketBundle\Entity\FoodType $foodType = null)
    {
        $this->foodType = $foodType;

        return $this;
    }

    /**
     * Get foodType
     *
     * @return \Trans\TicketBundle\Entity\FoodType
     */
    public function getFoodType()
    {
        return $this->foodType;
    }

    /**
     * Set hotelChildren
     *
     * @param \Trans\TicketBundle\Entity\HotelChildrenAge $hotelChildren
     * @return OrderTour
     */
    public function setHotelChildrenAge(\Trans\TicketBundle\Entity\HotelChildrenAge $hotelChildrenAge = null)
    {
        $this->hotelChildrenAge = $hotelChildrenAge;

        return $this;
    }

    /**
     * Get hotelChildren
     *
     * @return \Trans\TicketBundle\Entity\HotelChildrenAge
     */
    public function getHotelChildrenAge()
    {
        return $this->hotelChildrenAge;
    }

    /**
     * @return string
     */
    public function getOtherProfile()
    {
        return $this->otherProfile;
    }

    /**
     * @param string $otherProfile
     */
    public function setOtherProfile($otherProfile)
    {
        $this->otherProfile = $otherProfile;
    }

    /**
     * @return int
     */
    public function getAdults()
    {
        return $this->adults;
    }

    /**
     * @param int $adults
     */
    public function setAdults($adults)
    {
        $this->adults = $adults;
    }

    /**
     * @return int
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param int $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }
}
