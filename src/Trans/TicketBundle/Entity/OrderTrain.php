<?php

namespace Trans\TicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderTrain
 *
 * @ORM\Table("order_train")
 * @ORM\Entity
 */
class OrderTrain extends BaseOrderTicket
{

    /**
     * @var boolean
     *
     * @ORM\Column(name="wagon_any", type="boolean", nullable=true)
     */
    private $wagonAny;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wagon_cb", type="boolean", nullable=true)
     */
    private $wagonCb;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wagon_coupe", type="boolean", nullable=true)
     */
    private $wagonCoupe;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wagon_lux", type="boolean", nullable=true)
     */
    private $wagonLux;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wagon_platzkart", type="boolean", nullable=true)
     */
    private $wagonPlatzkart;


    /**
     * @var DocumentType
     * @ORM\ManyToOne(targetEntity="Trans\TicketBundle\Entity\DocumentType")
     */
    private $documentType;

    /**
     * @var String
     * @ORM\Column(name="document_number", type="string", length=50)
     */
    private $documentNumber;


    /**
     * @var String
     * @ORM\Column(name="document_info", type="string", length=100)
     */
    private $documentInfo;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set цwagonAny
     *
     * @param boolean $wagonAny
     * @return OrderTrain
     */
    public function setwagonAny($wagonAny)
    {
        $this->wagonAny = $wagonAny;

        return $this;
    }

    /**
     * Get wagonAny
     *
     * @return boolean 
     */
    public function getwagonAny()
    {
        return $this->wagonAny;
    }

    /**
     * Set wagonCb
     *
     * @param boolean $wagonCb
     * @return OrderTrain
     */
    public function setWagonCb($wagonCb)
    {
        $this->wagonCb = $wagonCb;

        return $this;
    }

    /**
     * Get wagonCb
     *
     * @return boolean 
     */
    public function getWagonCb()
    {
        return $this->wagonCb;
    }

    /**
     * Set wagonCoupe
     *
     * @param boolean $wagonCoupe
     * @return OrderTrain
     */
    public function setWagonCoupe($wagonCoupe)
    {
        $this->wagonCoupe = $wagonCoupe;

        return $this;
    }

    /**
     * Get wagonCoupe
     *
     * @return boolean 
     */
    public function getWagonCoupe()
    {
        return $this->wagonCoupe;
    }

    /**
     * Set wagonLux
     *
     * @param boolean $wagonLux
     * @return OrderTrain
     */
    public function setWagonLux($wagonLux)
    {
        $this->wagonLux = $wagonLux;

        return $this;
    }

    /**
     * Get wagonLux
     *
     * @return boolean 
     */
    public function getWagonLux()
    {
        return $this->wagonLux;
    }

    /**
     * Set wagonPlatzkart
     *
     * @param boolean $wagonPlatzkart
     * @return OrderTrain
     */
    public function setWagonPlatzkart($wagonPlatzkart)
    {
        $this->wagonPlatzkart = $wagonPlatzkart;

        return $this;
    }

    /**
     * Get wagonPlatzkart
     *
     * @return boolean 
     */
    public function getWagonPlatzkart()
    {
        return $this->wagonPlatzkart;
    }

    /**
     * @return String
     */
    public function getDocumentNumber()
    {
        return $this->documentNumber;
    }

    /**
     * @param String $documentNumber
     */
    public function setDocumentNumber($documentNumber)
    {
        $this->documentNumber = $documentNumber;
    }

    /**
     * @return String
     */
    public function getDocumentInfo()
    {
        return $this->documentInfo;
    }

    /**
     * @param String $documentInfo
     */
    public function setDocumentInfo($documentInfo)
    {
        $this->documentInfo = $documentInfo;
    }

    /**
     * @return \Trans\TicketBundle\Entity\DocumentType
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * @param \Trans\TicketBundle\Entity\DocumentType $documentType
     */
    public function setDocumentType(DocumentType $documentType)
    {
        $this->documentType = $documentType;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
