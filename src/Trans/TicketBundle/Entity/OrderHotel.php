<?php

namespace Trans\TicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Trans\MainBundle\Entity\City;

/**
 * OrderHotel
 *
 * @ORM\Table("order_hotel")
 * @ORM\Entity
 */
class OrderHotel extends BaseOrder
{

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Trans\MainBundle\Entity\City")
     */
    private $city;

    /**
     * @var integer
     *
     * @ORM\Column(name="adults", type="smallint")
     */
    private $adults;

    /**
     * @var integer
     *
     * @ORM\Column(name="children",type="smallint")
     */
    private $children;


    /**
     * @var HotelPlacement
     * @ORM\ManyToOne(targetEntity="Trans\TicketBundle\Entity\HotelPlacement")
     */
    private $placement;

    /**
     * @var HotelCategory
     * @ORM\ManyToOne(targetEntity="Trans\TicketBundle\Entity\HotelCategory")
     */
    private $category;

    /**
     * @var HotelChildrenAge
     * @ORM\ManyToOne(targetEntity="Trans\TicketBundle\Entity\HotelChildrenAge")
     */
    private $childrenAge;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set city
     *
     * @param City $city
     * @return OrderHotel
     */
    public function setCity(City $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set numberOfPeople
     *
     * @param integer $numberOfPeople
     * @return OrderHotel
     */
    public function setAdults($adults)
    {
        $this->adults = $adults;

        return $this;
    }

    /**
     * Get adults
     *
     * @return integer
     */
    public function getAdults()
    {
        return $this->adults;
    }

    /**
     * Set children
     *
     * @param integer $children
     * @return OrderHotel
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return integer
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return \Trans\TicketBundle\Entity\HotelPlacement
     */
    public function getPlacement()
    {
        return $this->placement;
    }

    /**
     * @param \Trans\TicketBundle\Entity\HotelPlacement $placement
     */
    public function setPlacement(HotelPlacement $placement)
    {
        $this->placement = $placement;
    }

    /**
     * @return \Trans\TicketBundle\Entity\HotelCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param \Trans\TicketBundle\Entity\HotelCategory $category
     */
    public function setCategory(HotelCategory $category)
    {
        $this->category = $category;
    }

    /**
     * @return \Trans\TicketBundle\Entity\HotelChildrenAge
     */
    public function getChildrenAge()
    {
        return $this->childrenAge;
    }

    /**
     * @param \Trans\TicketBundle\Entity\HotelChildrenAge $childrenAge
     */
    public function setChildrenAge($childrenAge)
    {
        $this->childrenAge = $childrenAge;
    }

    public function __toString()
    {
        return $this->name;
    }
}
