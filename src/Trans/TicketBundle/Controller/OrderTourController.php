<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/14/14
 * Time: 1:15 PM
 */

namespace Trans\TicketBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Trans\TicketBundle\Entity\OrderTour;
use Trans\TicketBundle\Form\Type\OrderTourType;

class OrderTourController extends Controller
{

    public function newAction(Request $request)
    {
        $tour = new OrderTour();
        $tourForm = $this->createForm(new OrderTourType($this->get('translator')), $tour);

        $tourForm->handleRequest($request);
        if ($tourForm->isValid()) {

            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($tour);
            $em->flush();

            $this->sendMail($tour);

            $this->get('session')->getFlashBag()->set('notice', 'Order Successful');

            return $this->redirect($request->getRequestUri());

        }

        return $this->render('TransTicketBundle:OrderTour:new.html.twig', array('form' => $tourForm->createView()));
    }

    private function sendMail(OrderTour $tour)
    {
        $receipents = array(
            $this->get('service_container')->getParameter('mailer_admin'),
            $this->get('service_container')->getParameter('mailer_tourism'),
            $this->get('service_container')->getParameter('mailer_sales_online')
        );
        foreach ($receipents as $receipent) {
            $message = \Swift_Message::newInstance()
                ->setSubject("Новый заказ путешествий")
                ->setFrom($this->get('service_container')->getParameter('mailer_user'))
                ->setTo($receipent)
                ->setBody($this->renderView('TransTicketBundle:OrderTour:message.html.twig', array('tour' => $tour)));

            $this->get("mailer")->send($message);
        }
    }
} 