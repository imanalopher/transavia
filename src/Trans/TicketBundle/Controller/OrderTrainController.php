<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/14
 * Time: 3:14 PM
 */

namespace Trans\TicketBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Trans\TicketBundle\Entity\OrderTrain;
use Trans\TicketBundle\Form\Type\OrderTrainType;

class OrderTrainController extends Controller
{

    public function newAction(Request $request)
    {

        $orderTrain = new OrderTrain();
        $form = $this->createForm(new OrderTrainType($this->get('translator')), $orderTrain);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($orderTrain);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Order Successful');

            $this->sendMail($orderTrain);

            return $this->redirect($request->getRequestUri());
        }


        return $this->render('TransTicketBundle:OrderTrain:new.html.twig', array('form' => $form->createView()));
    }

    private function sendMail(OrderTrain $orderTrain)
    {
        $receipents = array(
            $this->get('service_container')->getParameter('mailer_admin'),
            $this->get('service_container')->getParameter('mailer_zhd'),
            $this->get('service_container')->getParameter('mailer_matveev'),
            $this->get('service_container')->getParameter('mailer_sales'),
            $this->get('service_container')->getParameter('mailer_sales_online')
        );
        foreach ($receipents as $receipent) {
            $message = \Swift_Message::newInstance()
                ->setSubject("Новый заказ Ж/Д билета")
                ->setFrom($this->get('service_container')->getParameter('mailer_user'))
                ->setTo($receipent)
                ->setBody($this->renderView('TransTicketBundle:OrderTrain:message.html.twig', array('train' => $orderTrain)));

            $this->get("mailer")->send($message);
        }
    }
} 