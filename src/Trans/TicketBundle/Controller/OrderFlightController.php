<?php

namespace Trans\TicketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Trans\TicketBundle\Entity\OrderFlight;
use Trans\TicketBundle\Entity\OrderType;
use Trans\TicketBundle\Form\Type\OrderFlightType;

class OrderFlightController extends Controller
{
    public function newAction(Request $request)
    {
        $orderFlight = new OrderFlight();
        $form = $this->createForm(new OrderFlightType($this->get('translator')), $orderFlight);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($orderFlight);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Order Successful');

            $this->sendMail($orderFlight);

            return $this->redirect($request->getRequestUri());
        }

        return $this->render('TransTicketBundle:OrderFlight:new.html.twig', array('form' => $form->createView()));
    }

    private function sendMail(OrderFlight $orderFlight)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject("Новый заказ авиабилета")
            ->setFrom($this->get('service_container')->getParameter('mailer_user'))
            ->setTo($this->get('service_container')->getParameter('mailer_admin'))
            ->setBody($this->renderView("@TransTicket/OrderFlight/message.html.twig", array('flight' => $orderFlight)));

        $this->get("mailer")->send($message);
    }
}
