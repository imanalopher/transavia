<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/14
 * Time: 5:12 PM
 */

namespace Trans\TicketBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Trans\TicketBundle\Entity\OrderHotel;
use Trans\TicketBundle\Form\Type\OrderHotelType;

class OrderHotelController extends Controller
{

    public function newAction(Request $request)
    {

        $hotel = new OrderHotel();
        $form = $this->createForm(new OrderHotelType($this->get('translator')), $hotel, array('action' => $this->generateUrl('order_hotel_new')));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hotel);
            $em->flush();

            $this->sendMail($hotel);

            $this->get('session')->getFlashBag()->add('notice', 'Order Successful');

            return $this->redirect($this->generateUrl('trans_hotel_homepage'));
        }


        return $this->render('TransTicketBundle:OrderHotel:new.html.twig', array('form' => $form->createView()));
    }

    private function sendMail(OrderHotel $orderHotel)
    {
        $receipents = array(
            $this->get('service_container')->getParameter('mailer_admin'),
            $this->get('service_container')->getParameter('mailer_booking'),
            $this->get('service_container')->getParameter('mailer_matveev'),
            $this->get('service_container')->getParameter('mailer_sales'),
            $this->get('service_container')->getParameter('mailer_sales_online')
        );
        foreach ($receipents as $receipent) {
            $message = \Swift_Message::newInstance()
                ->setSubject("Новый заказ отеля")
                ->setFrom($this->get('service_container')->getParameter('mailer_user'))
                ->setTo($receipent)
                ->setBody($this->renderView('TransTicketBundle:OrderHotel:message.html.twig', array('hotel' => $orderHotel)));

            $this->get("mailer")->send($message);
        }
    }
} 