<?php

namespace Trans\TicketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BannerController extends Controller
{
    public function getBannerAction($bannerPosition)
    {
        $repository = $this -> getDoctrine()
            ->getRepository("TransTicketBundle:Banners");

        $banner = $repository -> findOneBy(array('position'=>$bannerPosition));

        if(!is_null($banner)){
            return $this->render('TransTicketBundle:Banner:index.html.twig', array('banner'=>$banner, 'position'=>$bannerPosition));
        }
        else
        {
            return new Response;
        }
    }
}
