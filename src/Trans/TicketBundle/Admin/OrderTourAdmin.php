<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/14/14
 * Time: 4:49 PM
 */

namespace Trans\TicketBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class OrderTourAdmin extends Admin
{

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('name')
            ->add('telephone')
            ->add('email')
            ->add('country')
            ->add('city')
            ->add('checkOut')
            ->add('checkIn')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'delete' => array(),
                    'show' => array(),
                )
            ));
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter
            ->add('name')
            ->add('telephone')
            ->add('checkOut')
            ->add('email')
            ->add('checkIn')
            ->add('country')
            ->add('city')
            ->add('hotelCategory')
            ->add('hotelPlacement')
            ->add('hotelChildren')
            ->add('foodType')
            ->add('profiles');
    }


} 