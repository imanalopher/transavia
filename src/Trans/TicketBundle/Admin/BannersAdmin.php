<?php

namespace Trans\TicketBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Trans\TicketBundle\DBAL\BannersPositionType;

class BannersAdmin extends Admin {


    protected function configureListFields(ListMapper $listmapper)
    {
        $listmapper
            ->addIdentifier('id',null, array('label'=>'id'))
            ->addIdentifier('name',null,array('label'=>'Название'))
            ->add('position', null, array('label'=>'Позиция'))
            ->add('active', 'boolean', array('editable' => true,'label' => 'Активен'));
    }

    protected function configureFormFields(FormMapper $formmapper)
    {
        $formmapper
            ->add('name', null, array('label'=>'Название', 'required'=> true))
            ->add('url', null,array('label'=>'Url', 'required'=>false))
            ->add('image', 'sonata_type_model_list', array('required'=> true, 'label'=> 'Рисунок'), array('link_parameters'=>array('context'=>'banner')))
            ->add('position', 'choice', array('label'=>'Позиция', 'required'=>true, 'choices'=>BannersPositionType::getArray()))
            ->add('active', null, array('label' => 'Активен'));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('url')
            ->add('position')
            ->add('active')
        ;
    }
} 