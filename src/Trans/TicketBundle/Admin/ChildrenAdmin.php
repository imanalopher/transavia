<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/26/14
 * Time: 11:20 AM
 */

namespace Trans\TicketBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ChildrenAdmin extends Admin
{

    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('name', null, array('label' => 'Children'));
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form->add('name', null, array('label' => 'Children'));
    }

} 