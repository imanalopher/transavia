<?php

namespace Trans\TicketBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class OrderTrainAdmin extends Admin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('callTime')
            ->add('departurePoint')
            ->add('destinationPoint')
            ->add('name')
            ->add('telephone')
            ->add('email')
            ->add('checkOut')
            ->add('checkIn')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'delete' => array(),
                )
            ));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with("Основные")
            ->add('name')
            ->add('telephone')
            ->add('email')
            ->add('checkOut')
            ->add('checkIn')
            ->add('departurePoint')
            ->add('destinationPoint')
            ->add('callTime')
            ->add('documentNumber')
            ->add('documentInfo')
            ->add('company')
            ->add('cheapest')
            ->add('roundTrip')
            ->end()
            ->with("Тип вагона")
            ->add('wagonAny')
            ->add('wagonCb')
            ->add('wagonCoupe')
            ->add('wagonLux')
            ->add('wagonPlatzkart')
            ->end()
            ->with("Категория пассажиров")
            ->add('passInfant')
            ->add('passChildren')
            ->add('passYoung')
            ->add('passAdult')
            ->add('passRetired')
            ->end();
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove("create");
    }

}
