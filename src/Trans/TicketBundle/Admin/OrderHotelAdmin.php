<?php

namespace Trans\TicketBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class OrderHotelAdmin extends Admin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('telephone')
            ->add('email')
            ->add('city')
            ->add('checkOut')
            ->add('checkIn')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'delete' => array(),
                    'show' => array(),
                )
            ));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('email')
            ->add('city')
            ->add('telephone')
            ->add('checkOut')
            ->add('checkIn')
            ->add('placement')
            ->add('category')
            ->add('adults')
            ->add('children')
            ->add('childrenAge');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove("create");
    }


}
