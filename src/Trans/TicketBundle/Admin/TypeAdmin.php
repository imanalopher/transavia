<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/18/14
 * Time: 5:49 PM
 */

namespace Trans\TicketBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TypeAdmin extends Admin {

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('translations','a2lix_translations',array('label'=>'Type Name'));
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter
            ->add('name');
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('translations', 'a2lix_translations');
    }


} 