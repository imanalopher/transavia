<?php

namespace Trans\TicketBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class OrderFlightAdmin extends Admin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper

            ->add('name')
            ->add('telephone')
            ->add('email')
            ->add('callTime')
            ->add('checkOut')
            ->add('checkIn')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with("Основные")
            ->add('name')
            ->add('telephone')
            ->add('email')
            ->add('checkOut')
            ->add('checkIn')
            ->add('departurePoint')
            ->add('destinationPoint')
            ->add('callTime')
            ->add('company')
            ->add('birthday')
            ->add('comment')
            ->add('cheapest')
            ->add('roundTrip')
            ->end()
            ->with("Категория пассажиров")
            ->add('passInfant')
            ->add('passChildren')
            ->add('passYoung')
            ->add('passAdult')
            ->add('passRetired')
            ->end()
            ->with("Класс билета")
            ->add('classAny')
            ->add('classEconomy')
            ->add('classBusiness')
            ->add('classFirst')
            ->end()
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove("create");
    }

}
