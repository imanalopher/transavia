<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/14
 * Time: 5:06 PM
 */

namespace Trans\TicketBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderHotelType extends BaseOrderType
{

    public function __construct($translator)
    {
        parent::__construct($translator);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('checkOut', 'date', array('label' => "hotel.Check Out",
                'widget' => 'single_text',
                'attr' => array('class' => 'b-input__text b-date-leave__input date-in-out',
                    'placeholder' => $this->translator->trans('p.arrival')
                )))
            ->add('checkIn', 'date', array('label' => "hotel.Check In",
                'widget' => 'single_text',
                'attr' => array('class' => 'b-input__text b-date-arrive__input date-in-out',
                    'placeholder' => $this->translator->trans('p.checkout')
                )))
            ->add('city', null, array('attr' => array('class' => 'b-input__select b-city__input')))
            ->add('adults', null, array('attr' => array('class' => 'b-passanger-number__input b-passanger-number__adult')))
            ->add('children', null, array('attr' => array('class' => 'b-passanger-number__input b-passanger-number__infant')))
            ->add('placement', null, array('required' => true, 'attr' => array('class' => 'b-input__select b-accomodation__input')))
            ->add('category', null, array('required' => true, 'attr' => array('class' => 'b-input__select b-accomodation__input')))
            ->add('childrenAge', null, array('label' => "hotel.Children Age", 'required' => false, 'attr' => array('class' => 'b-input__select b-age__input')));
    }


    public function getName()
    {
        return "order_hotel";
    }
}