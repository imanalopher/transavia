<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/14/14
 * Time: 4:09 PM
 */

namespace Trans\TicketBundle\Form\Type;


use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderTourType extends BaseOrderType
{

    public function __construct(Translator $translator)
    {
        parent::__construct($translator);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('checkOut', 'date', array('label' => "hotel.Check Out",
                'widget' => 'single_text',
                'attr' => array('class' => 'b-input__text b-date-leave__input date-in-out',
                    'placeholder' => $this->translator->trans('p.arrival')
                )))
            ->add('checkIn', 'date', array('label' => "hotel.Check In",
                'widget' => 'single_text',
                'attr' => array('class' => 'b-input__text b-date-arrive__input date-in-out',
                    'placeholder' => $this->translator->trans('p.checkout')
                )))
            ->add('profiles', 'entity', array('class' => 'Trans\TicketBundle\Entity\TourProfile', 'property' => 'name', 'multiple' => true, 'expanded' => true))
            ->add('otherProfile', null)
            ->add('city', null)
            ->add('country')
            ->add('adults', null, array('attr' => array('class' => 'b-passanger-number__input b-passanger-number__adult')))
            ->add('children', null, array('attr' => array('class' => 'b-passanger-number__input b-passanger-number__infant')))
            ->add('foodType', null, array('label' => "Food Type",'attr'=>array('class'=>'b-input__select b-accomodation__input')))
            ->add('hotelPlacement', null, array('label' => "Hotel Placement",'attr'=>array('class'=>'b-input__select b-accomodation__input')))
            ->add('hotelChildrenAge', null, array('label' => 'Hotel Children','attr'=>array('class'=>'b-input__select b-age__input')))
            ->add('hotelCategory', null, array('label' => 'Hotel Category','attr'=>array('class'=>'b-input__select b-accomodation__input')));
    }


    public function getName()
    {
        return "order_tour";
    }
}