<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/14
 * Time: 1:33 AM
 */

namespace Trans\TicketBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BaseOrderTicketType extends BaseOrderType
{


    public function __construct($translator)
    {
        parent::__construct($translator);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('checkOut', 'date', array('label' => "Check Out",
                'widget' => 'single_text',
                'attr' => array('class' => 'b-input__text b-date-leave__input date-in-out',
                    'placeholder' => $this->translator->trans('p.arrival')
                )))
            ->add('checkIn', 'date', array('label' => "Check In",
                'widget' => 'single_text',
                'attr' => array('class' => 'b-input__text b-date-arrive__input date-in-out',
                    'placeholder' => $this->translator->trans('p.checkout')
                )))
            ->add('callTime', null, array('required' => true,
                'attr' => array(
                    'class' => 'b-input__text b-call-time__input',
                    'placeholder' => $this->translator->trans('p.calltime')
                )))
            ->add('departurePoint', null, array('required' => true,
                'attr' => array(
                    'class' => 'b-input__text b-leave-from__input',
                    'placeholder' => $this->translator->trans('p.departure')
                )))
            ->add('destinationPoint', null, array('required' => true,
                'attr' => array(
                    'class' => 'b-input__text b-destination__input',
                    'placeholder' => $this->translator->trans('p.destination')
                )))
            ->add('company', null, array('required' => false, 'attr' => array(
                'class' => 'b-input__text b-comp-name__input',
                'placeholder' => $this->translator->trans('p.company')
            )))
            ->add('cheapest', 'checkbox', array('required' => false))
            ->add('roundTrip', 'checkbox', array('required' => false))
            ->add('passInfant', 'checkbox', array('required' => false))
            ->add('passChildren', 'checkbox', array('required' => false))
            ->add('passYoung', 'checkbox', array('required' => false))
            ->add('passAdult', 'checkbox', array('required' => false))
            ->add('passRetired', 'checkbox', array('required' => false))
            ->add('payment', 'entity', array(
                'class' => 'Trans\TicketBundle\Entity\Payment',
                'property' => 'name', 'required' => true,
                'attr' => array('class' => 'b-input__select b-pay-way__input')
            ));
    }

    public function getName()
    {
        return "base_order_ticket";
    }
}