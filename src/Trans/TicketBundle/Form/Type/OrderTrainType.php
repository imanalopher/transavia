<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/14
 * Time: 3:07 PM
 */

namespace Trans\TicketBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderTrainType extends BaseOrderTicketType
{
    public function __construct($translator)
    {
        parent::__construct($translator);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('wagonAny', null, array('attr' => array('class' => 'checkbox')))
            ->add('wagonCb', null, array('attr' => array('class' => 'checkbox')))
            ->add('wagonCoupe', null, array('attr' => array('class' => 'checkbox')))
            ->add('wagonLux', null, array('attr' => array('class' => 'checkbox')))
            ->add('wagonPlatzkart', null, array('attr' => array('class' => 'checkbox')))
            ->add('documentType', 'entity', array(
                'class' => 'Trans\TicketBundle\Entity\DocumentType',
                'property' => 'name',
                'attr' => array('class' => 'b-input__select b-pay-way__input'
                )))
            ->add('documentNumber', null, array('required' => true,
                'attr' => array(
                    'class' => 'b-input__text b-doc-num__input',
                    'placeholder' => $this->translator->trans('p.document.num')
                )))
            ->add('documentInfo', null, array('required' => true,
                'attr' => array('class' => 'b-input__text b-doc-given__input',
                    'placeholder' => $this->translator->trans('p.document.info')
                )));
    }


    public function getName()
    {
        return "order_train";
    }
}