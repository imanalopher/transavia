<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/14
 * Time: 1:48 AM
 */

namespace Trans\TicketBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderFlightType extends BaseOrderTicketType
{

    public function __construct($translator)
    {
        parent::__construct($translator);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('birthday', 'date', array('required' => false))
            ->add('classAny', 'checkbox', array('required' => false))
            ->add('classEconomy', 'checkbox', array('required' => false))
            ->add('classBusiness', 'checkbox', array('required' => false))
            ->add('classFirst', 'checkbox', array('required' => false))
            ->add('orderType', 'entity', array(
                'class' => 'Trans\TicketBundle\Entity\OrderType',
                'property' => 'name'
            ));
    }


    public function getName()
    {
        return 'order_flight';
    }
}