<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/14
 * Time: 1:30 AM
 */

namespace Trans\TicketBundle\Form\Type;


use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BaseOrderType extends AbstractType
{

    protected $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                'attr' => array('class' => 'b-input__text b-fullname__input',
                    'placeholder' => $this->translator->trans('Name')
                )))
            ->add('telephone', null, array(
                'required' => true,
                'attr' => array('class' => 'b-input__text b-tel__input',
                    'placeholder' => $this->translator->trans('p.telephone'))))
            ->add('email', 'email', array('required' => true,
                'attr' => array('class' => 'b-input__text b-email__input',
                    'placeholder' => $this->translator->trans('p.email')
                )))
            ->add('submit', 'submit', array('attr' => array('class' => 'b-button')));
    }


    public function getName()
    {
        return 'base_order';
    }
}