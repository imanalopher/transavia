<?php

namespace Trans\TicketBundle\DBAL;

use Strokit\CoreBundle\DBAL\EnumType;

class BannersPositionType extends EnumType
{
    protected $name = 'enumbannersposition';
    protected $values = array('main-big-left'=>"main-big-left", 'main-big-right'=>'main-big-right', 'main-mini'=>'main-mini', 'order-tour'=>'order-tour', 'contacts'=>'contacts');

    public static function getArray()
    {
        return array('main-big-left'=>"main-big-left", 'main-big-right'=>'main-big-right', 'main-mini'=>'main-mini', 'order-tour'=>'order-tour', 'contacts'=>'contacts');
    }
}