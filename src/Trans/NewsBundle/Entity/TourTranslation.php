<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/22/14
 * Time: 11:20 AM
 */

namespace Trans\NewsBundle\Entity;


use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;
use Doctrine\ORM\Mapping as ORM;
use Prezent\Doctrine\Translatable\Annotation as Prezent;

/**
 * @ORM\Table("tour_translation")
 * @ORM\Entity
 */
class TourTranslation extends AbstractTranslation
{

    /**
     * @Prezent\Translatable(targetEntity="Trans\NewsBundle\Entity\Tour")
     */
    protected $translatable;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="text")
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="full_description", type="text")
     */
    private $fullDescription;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullDescription()
    {
        return $this->fullDescription;
    }

    /**
     * @param string $fullDescription
     */
    public function setFullDescription($fullDescription)
    {
        $this->fullDescription = $fullDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

} 