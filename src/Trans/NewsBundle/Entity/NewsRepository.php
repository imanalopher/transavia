<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/24/14
 * Time: 12:06 AM
 */

namespace Trans\NewsBundle\Entity;


use Doctrine\ORM\EntityRepository;

class NewsRepository extends EntityRepository
{

    public function findByNot($id)
    {
        return $this->createQueryBuilder('n')
            ->where('n.id != :id')
            ->andWhere('n.active = 1')
            ->setMaxResults(2)
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function findLatest($limit)
    {
        return $this->createQueryBuilder('n')
            ->join('n.section', 's')
            ->where('s.name = :name')
            ->andWhere('n.active = 1')
            ->setMaxResults($limit)
            ->addOrderBy('n.createdAt', 'DESC')
            ->setParameter('name', News::COMPANY_NEWS)
            ->getQuery()
            ->getResult();
    }

} 