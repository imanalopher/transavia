$(function(){
    $('#load-more-news').on('click', function(e){
        e.preventDefault();
        var that = $(this);

        $.getJSON(Routing.generate('trans_news_loadNews', {'_locale':'ru', 'page':that.data('page')}), function(data){
            if (data.html != '') {
                $('#container').append(data.html).masonry('reloadItems');
                that.data('page', data.page).attr('data-page', data.page);
                $container.masonry('destroy');
                setTimeout(updateRows,2000);
            }else{
                that.unbind('click');
            }
        });
    });
    setTimeout(updateRows,1500);
});

var $container = $('#container');
function updateRows(){
    // initialize
    $container.masonry({
        columnWidth: 20,
        itemSelector: '.b-news-list__item',
        singleMode: true
    });
}