<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/22/14
 * Time: 10:42 AM
 */

namespace Trans\NewsBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class NewsAdmin extends Admin
{

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('active', null, array('required' => false))
            ->add('section')
            ->add('createdAt','datetime',array('label'=>'Дата создания'))
            ->add('media', 'sonata_media_type', array(
                'provider' => 'sonata.media.provider.image',
                'context' => 'news',
                'required' => false
            ))
            ->add('translations', 'a2lix_translations', array(
                'fields' => array(
                    'title' => array(),
                    'shortDescription' => array(
                        'field_type' => 'ckeditor'
                    ),
                    'fullDescription' => array(
                        'field_type' => 'ckeditor'
                    )
                )
            ));
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('active')
            ->add('section')
            ->add('translations', 'a2lix_translations')
            ->add('_action', 'actions', array('actions' => array('show' => array(), 'edit' => array(), 'delete' => array())
            ));
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter->add('active')
            ->add('title')
            ->add('section')
            ->add('media', 'sonata_media_type')
            ->add('shortDescription', null, array('safe' => true, 'label' => 'Краткое описание'))
            ->add('fullDescription', null, array('safe' => true, 'label' => 'Полное описание'));
    }


} 