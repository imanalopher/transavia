<?php
/**
 * Created by PhpStorm.
 * User: bupychuk
 * Date: 15.06.14
 * Time: 17:17
 */

namespace Trans\NewsBundle\Controller;


use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TourController extends Controller {

    public function listAction()
    {
        $tours = $this->getDoctrine()->getRepository('TransNewsBundle:Tour')->findBy(array('active'=>true));
        return $this->render('TransNewsBundle:Tour:list.html.twig',array('tours'=>$tours));
    }

    public function tourAction($id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $tour = $em->getRepository('TransNewsBundle:Tour')->find($id);
        if (!$tour) {
            throw new EntityNotFoundException();
        }
        return $this->render('@TransNews/Tour/tour.html.twig', array('tour' => $tour));
    }
} 