<?php

namespace Trans\NewsBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Trans\NewsBundle\Entity\News;

class NewsController extends Controller
{
    public function latestAction()
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $news = $em->getRepository('TransNewsBundle:News')->findLatest($this->get('service_container')->getParameter('news_limit'));

        return $this->render('@TransNews/News/latest.html.twig', array('news' => $news));
    }

    public function indexAction()
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $news = $em->getRepository('TransNewsBundle:News')->findBy(array('active' => true), array('createdAt' => 'DESC'));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator
            ->paginate($news,
                1,
                $this->get('service_container')->getParameter('articles_limit')
            );

        return $this->render('@TransNews/News/index.html.twig', array('news' => $pagination));
    }

    public function newsAction($id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $news = $em->getRepository('TransNewsBundle:News')->find($id);
        $newsNot = $em->getRepository('TransNewsBundle:News')->findByNot($id);
        if (!$news) {
            throw new EntityNotFoundException();
        }
        return $this->render('@TransNews/News/news.html.twig', array('news' => $news, 'newsNot' => $newsNot));
    }

    public function loadNewsAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new HttpException('Request must be a ajax request');
        }

        $page = $request->get('page', 1);

        $em = $this->get('doctrine.orm.entity_manager');
        $news = $em->getRepository('TransNewsBundle:News')->findBy(array('active' => true), array('createdAt' => 'DESC'));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator
            ->paginate($news,
                $page,
                $this->get('service_container')->getParameter('articles_limit')
            );

        return new JsonResponse(array(
            'html' => $this->renderView('TransNewsBundle:News:_extraNews.html.twig', array(
                    'news' => $pagination
                )),
            'page' => $page + 1
        ));
    }
}
