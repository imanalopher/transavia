<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/25/14
 * Time: 9:20 PM
 */

namespace Trans\NewsBundle\DataFixtures;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\NewsBundle\Entity\News;

class LoadNewsData extends AbstractFixture implements OrderedFixtureInterface
{


    function load(ObjectManager $manager)
    {
        $values = array(
            array(
                'title' => 'Авиакомпания Air Arabia Kids Go Free',
                'sDescription' => 'Уважаемые Друзья! Напоминаем Вам об акции. Авиакомпания Air Arabia Kids Go Free: дети, путешествующие с взрослыми рейсами а/к Air Arabia в ОАЭ, получают бесплатные авиабилеты.',
                'fDescription' => 'Так получилось, что на нашем проекте сейчас используется Ansible. Я не буду останавливаться на том, что такое Ansible, как его готовить и с чем употреблять, а также на том, почему используется именно он (ибо выбор этот для условий эксплуатации под ОС от Microsoft оптимальным не назовешь) — в контексте этого поста предлагаю считать это данностью. Также ни для кого не секрет, что очень много веб-разработчиков по разным причинам работает под Windows. Нежелание сражаться с линуксами, нехватка денег на мак, танчики после работы, корпоративная политика — причин тоже может быть вагон. И о них в этом посте тоже не будет ни слова.',
                'section' => $this->getReference(News::COMPANY_NEWS)
            ),
            array(
                'title' => 'BEK AIR рада сообщить о начале выполнения прямых рейсов ',
                'sDescription' => 'BEK AIR рада сообщить о начале выполнения прямых рейсов по маршруту Алматы - Актобе - Алматы и Актобе - Уральск - Актобе с 17 мая 2014г.',
                'fDescription' => 'Забегая вперед, скажу, что для меня это был первый опыт работы с Cygwinе, эта инстукция в целом — попытка дать возможность команде работать с опрометчиво выбранным без скидки на Windows инстументом) и установка пакетов там оказалась штукой не совсем для меня очевидной. Пришлось загуглить: оказывается, чтобы установить пакет, нужно, найдя его в списке, нажать вот на эту иконку — тогда изменится его статус; чтобы добавить или удалить пакеты, нужно запустить инсталлятор и идти тем же путем, что и при начальной установке.',
                'section' => $this->getReference(News::COMPANY_NEWS)
            ),
        );
        foreach ($values as $value) {
            $news = new News();
            $news->setActive(true)
                ->setTitle($value['title'])
                ->setShortDescription($value['sDescription'])
                ->setFullDescription($value['fDescription'])
                ->setSection($value['section'])
                ->setCreatedAt(new \DateTime('now'));
            $manager->persist($news);
        }
        $manager->flush();

    }


    function getOrder()
    {
        return 2;
    }
}