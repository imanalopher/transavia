<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/22/14
 * Time: 10:53 AM
 */

namespace Trans\NewsBundle\DataFixtures;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\NewsBundle\Entity\News;
use Trans\NewsBundle\Entity\Section;

class LoadSectionData extends AbstractFixture implements OrderedFixtureInterface
{


    function load(ObjectManager $manager)
    {
        $values = array(News::COMPANY_NEWS, 'Новости в мире');
        foreach ($values as $value) {
            $section = new Section();
            $section->setName($value);
            $this->addReference($value, $section);
            $manager->persist($section);
        }
        $manager->flush();
    }


    function getOrder()
    {
        return 1;
    }
}