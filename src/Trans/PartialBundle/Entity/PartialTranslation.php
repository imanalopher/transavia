<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/21/14
 * Time: 3:12 PM
 */

namespace Trans\PartialBundle\Entity;


use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("partial_translation")
 * @ORM\Entity
 */
class PartialTranslation extends AbstractTranslation
{

    /**
     * @Prezent\Translatable(targetEntity="Trans\PartialBundle\Entity\Partial")
     */
    protected $translatable;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * Set content
     *
     * @param string $content
     * @return Partial
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public static function getTranslationEntityClass()
    {
        return 'PartialTranslation';
    }
} 