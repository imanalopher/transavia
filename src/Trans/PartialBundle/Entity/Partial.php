<?php

namespace Trans\PartialBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslatable;
use Prezent\Doctrine\Translatable\Annotation as Prezent;

/**
 * Partial
 *
 * @ORM\Table("partial_partial")
 * @ORM\Entity
 */
class Partial extends AbstractTranslatable
{

    const CONTACTS = "contacts";

    const ABOUT_COMPANY = "about-company";

    const CENTRAL_OFFICE = 'central-office';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="partial_id", type="string", length=255)
     */
    private $partial_id;

    /**
     * @var Section
     * @ORM\ManyToOne(targetEntity="Trans\PartialBundle\Entity\Section")
     */
    private $section;

    /**
     * @param \Trans\PartialBundle\Entity\Section $section
     */
    public function setSection($section)
    {
        $this->section = $section;
    }

    /**
     * @return \Trans\PartialBundle\Entity\Section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @Prezent\Translations(targetEntity="Trans\PartialBundle\Entity\PartialTranslation")
     */
    protected $translations;

    /**
     * @Prezent\CurrentLocale
     */
    private $currentLocale;

    private $currentTranslation;


    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set partial_id
     *
     * @param string $partialId
     * @return Partial
     */
    public function setPartialId($partialId)
    {
        $this->partial_id = $partialId;

        return $this;
    }

    /**
     * Get partial_id
     *
     * @return string
     */
    public function getPartialId()
    {
        return $this->partial_id;
    }

    public function getTitle()
    {
        return $this->translate()->getTitle();
    }

    public function setTitle($title)
    {
        $this->translate()->setTitle($title);
        return $this;
    }

    public function getContent()
    {
        return $this->translate()->getContent();
    }

    public function setContent($content)
    {
        $this->translate()->setContent($content);
        return $this;
    }


    public function __toString()
    {
        return $this->getTitle();
    }


    public function translate($locale = null)
    {
        if (null === $locale) {
            $locale = $this->currentLocale;
        }

        if (!$locale) {
            //TODO: change it
            $locale = "ru";
        }

        if ($this->currentTranslation && $this->currentTranslation->getLocale() === $locale) {
            return $this->currentTranslation;
        }

        if (!$translation = $this->translations->get($locale)) {
            $translation = new PartialTranslation();
            $translation->setLocale($locale);
            $this->addTranslation($translation);
        }

        $this->currentTranslation = $translation;
        return $translation;
    }

    public static function getTranslationEntityClass()
    {
        return 'PartialTranslation';
    }
}
