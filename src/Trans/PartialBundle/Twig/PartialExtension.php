<?php

namespace Trans\PartialBundle\Twig;


use Doctrine\ORM\EntityManager;

class PartialExtension extends \Twig_Extension
{

    private $entityManager;

    public function  __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFunctions()
    {
        return array(
            'partial' => new \Twig_Function_Method($this, 'partial')
        );
    }

    public function partial($partial_id)
    {
        $partial = $this->entityManager->getRepository('TransPartialBundle:Partial')->findOneBy(array('partial_id' => $partial_id));
        return ($partial) ? $partial->getContent() : '';
    }

    public function getName()
    {
        return 'partial_extension';
    }
}