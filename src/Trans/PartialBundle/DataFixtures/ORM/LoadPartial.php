<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/21/14
 * Time: 11:04 AM
 */

namespace Trans\PartialBundle\DataFixtures;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\PartialBundle\Entity\Partial;

class LoadPartial extends AbstractFixture implements OrderedFixtureInterface
{


    function load(ObjectManager $manager)
    {
        $partials = array(
            array('partial_id' => Partial::ABOUT_COMPANY,
                'title_ru' => 'Титул',
                'content_ru' => '					<p>
						Трансавиа является лидирующим агентством по предоставлению туристических услуг на рынке Казахстана. Компания основана в 2000г., многолетний опыт и высокий уровень профессионализма сотрудников позволяет нам всегда двигаться вперед.
					</p>
					<p>
						Мы предлагаем Вам надежный сервис, круглосуточный офис 24 часа, низкие цены и удобную форму оплаты.
					</p>
					<p>
						На нашем сайте Вы можете забронировать и приобрести авиабилеты онлайн по самым низким ценам, а также у Вас есть возможность оплаты не только кредитной картой, но и в офисах продаж в 18 городах Казахстана.
					</p>',
                'section' => $this->getReference('section_1')
            ),
            array('partial_id' => Partial::CONTACTS,
                'title_ru' => 'Контакты',
                'content_ru' => '<ul>
						<li class="b-footer__contacts__item b-footer__phone">
							<span class="b-footer__tel-code">+7 (727)</span> 315 04 14
						</li>
						<li class="b-footer__contacts__item b-footer__mail">
							<a href="mailto:info@transavia-travel.kz">info@transavia-travel.kz</a>
						</li>
						<li class="b-footer__contacts__item b-footer__fax">
							<span class="tel-code">+7 (727)</span> 315 04 13
						</li>
						<li class="b-footer__contacts__item b-footer__address-all">
							<span class="red underline underline-red">Телефоны и адреса всех офисов</span>
						</li>
					</ul>',
                'section' => $this->getReference('section_2')
            ),
            array('partial_id' => 'slogan-security',
                'title_ru' => 'Безопаспость',
                'content_ru' => 'Безопасность превыше всего! Путешествуя вместе с нами Вы получите приятные эмоции и чувство
                защищености в любой точки мира.'
            , 'section' => $this->getReference('section_1')
            ),
            array('partial_id' => 'slogan-time',
                'title_ru' => 'Время',
                'content_ru' => 'Время - деньги! Мы понимаем что динамическое развитие бизнеса требует быстрого обслуживания и четких
                решений.',
                'section' => $this->getReference('section_1')
            ),
            array('partial_id' => 'slogan-quality',
                'title_ru' => 'Качество',
                'content_ru' => 'Качество залог успеха. Мы предоставляем качественные услуги нашим клиентам вот уже 14 лет, и
                являемся лидерами отрасли.',
                'section' => $this->getReference('section_1')
            ),
            array(
                'partial_id' => 'central-office',
                'title_ru' => 'центральный офис',
                'content_ru' => '<p>
                10000 Казахстан, Алматы, Керемет 5, ул. Сейфуллина, уг. Никитина (ниже Тимирязева, заезд с Сейфуллина)
            </p>

            <p>
                часы работы: с 09.00 – 19.00 <span class="regime regime-1"></span>
            </p>


            <div class="b-contacts-list">
                <ul>
                    <li class="b-contacts-list__item b-contacts__phone">
                        <span class="b-footer__tel-code">+7 (727)</span> 315 04 14
                    </li>
                    <li class="b-contacts-list__item b-contacts__mail">
                        <a href="mailto:info@transavia-travel.kz">info@transavia-travel.kz</a>
                    </li>
                    <li class="b-contacts-list__item b-contacts__fax">
                        <span class="tel-code">+7 (727)</span> 315 04 13
                    </li>
                    <li class="b-contacts-list__item b-contacts__address-all">
                        <span class="red underline underline-red"><a href="#">Телефоны и адреса всех офисов</a></span>
                    </li>
                </ul>
            </div>',
                'section' => $this->getReference('contacts')
            ),
            array(
                'partial_id' => 'air-booking',
                'title_ru' => 'авиа касса',
                'content_ru' => '',
                'section' => $this->getReference('contacts')
            ),
            array(
                'partial_id' => 'train-booking',
                'title_ru' => 'ж/д касса',
                'content_ru' => '',
                'section' => $this->getReference('contacts')
            ),
            array(
                'partial_id' => 'corporate-department',
                'title_ru' => 'корпоративный отдел',
                'content_ru' => '',
                'section' => $this->getReference('contacts')
            ),
            array(
                'partial_id' => 'tourist-department',
                'title_ru' => 'туристический отдел',
                'content_ru' => '',
                'section' => $this->getReference('contacts')
            ),
            array(
                'partial_id' => 'cargo-department',
                'title_ru' => 'грузовой отдел',
                'content_ru' => '',
                'section' => $this->getReference('contacts')
            ),
            array(
                'partial_id' => 'charter-department',
                'title_ru' => 'отдел чартерных перевозок',
                'content_ru' => '',
                'section' => $this->getReference('contacts')
            ),
            array(
                'partial_id' => 'finance-department',
                'title_ru' => 'финансовый отдел',
                'content_ru' => '',
                'section' => $this->getReference('contacts')
            ),
            array(
                'partial_id'=>'hotel-index',
                'title_ru'=>'Гостиницы',
                'content_ru'=>'<p>Агентство "Трансавиа" предлагает услуги по бронированию гостиниц в городах Казахстана:</p>',
                'section'=>$this->getReference('section_1')
            )
        );

        foreach ($partials as $p) {
            $partial = new Partial();
            $partial->setPartialId($p['partial_id']);
            $partial->setSection($p['section']);

            $partial->translate('ru')->setTitle($p['title_ru']);

            $partial->translate('ru')->setContent($p['content_ru']);
            $manager->persist($partial);

        }
        $manager->flush();
    }

    function getOrder()
    {
        return 2;
    }
}