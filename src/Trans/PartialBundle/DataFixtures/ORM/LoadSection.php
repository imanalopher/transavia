<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/22/14
 * Time: 10:10 AM
 */

namespace Trans\PartialBundle\DataFixtures;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\PartialBundle\Entity\Section;

class LoadSection extends AbstractFixture implements OrderedFixtureInterface
{


    function load(ObjectManager $manager)
    {
        $sections = array('section_1', 'section_2', 'contacts');
        foreach ($sections as $s) {
            $section = new Section();
            $section->setName($s);
            $manager->persist($section);
            $this->addReference($s, $section);
        }
        $manager->flush();

    }


    function getOrder()
    {
        return 1;
    }
}