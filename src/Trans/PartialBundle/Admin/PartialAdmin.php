<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/21/14
 * Time: 10:40 AM
 */

namespace Trans\PartialBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class PartialAdmin extends Admin
{

    protected function configureListFields(ListMapper $list)
    {
        $list->add('translations', 'a2lix_translations')
            ->add('_action', 'actions', array('actions' => array('edit' => array(), 'show' => array())));
    }

    protected function configureFormFields(FormMapper $mapper)
    {
        if ($this->getSubject()->getId() == null)
            $mapper->add('partial_id');
        $mapper->add('translations', 'a2lix_translations', array(
            'fields' => array(
                'title' => array(
                    //'attr' => array('readonly' => 'readonly')
                ),
                'content' => array(
                    'field_type' => 'ckeditor',
                    'config' => array('allowedContent' => true)
                )
            )
        ));
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter->add('partial_id')
            ->add('title')
            ->add('content');
    }


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
//        $collection->remove('create');
    }


} 