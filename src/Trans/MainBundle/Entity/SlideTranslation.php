<?php

namespace Trans\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;
use Prezent\Doctrine\Translatable\Annotation as Prezent;

/**
 * SlideTranslation
 *
 * @ORM\Table(name="main_slide_translation")
 * @ORM\Entity
 */
class SlideTranslation extends AbstractTranslation
{

    /**
     * @Prezent\Translatable(targetEntity="Trans\MainBundle\Entity\Slide")
     */
    protected $translatable;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;


    public function __toString()
    {
        return $this->getTitle();
    }

    public static function getTranslationEntityClass()
    {
        return 'SlideTranslation';
    }


    /**
     * Set title
     *
     * @param string $title
     * @return SlideTranslation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
