<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/25/14
 * Time: 2:36 PM
 */

namespace Trans\MainBundle\Entity;


use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("city_translation")
 * @ORM\Entity
 */
class CityTranslation extends AbstractTranslation
{

    /**
     * @Prezent\Translatable(targetEntity="Trans\MainBundle\Entity\City")
     */
    protected $translatable;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=10)
     */
    private $name;

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public static function getTranslationEntityClass()
    {
        return 'CityTranslation';
    }
} 