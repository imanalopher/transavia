<?php

namespace Trans\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslatable;

/**
 * City
 *
 * @ORM\Table("city")
 * @ORM\Entity
 */
class City extends AbstractTranslatable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Trans\MainBundle\Entity\Hotel", mappedBy="city")
     */
    private $hotels;

    /**
     * @Prezent\Translations(targetEntity="Trans\MainBundle\Entity\CityTranslation")
     */
    protected $translations;

    /**
     * @Prezent\CurrentLocale
     */
    private $currentLocale;

    private $currentTranslation;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->hotels = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->translate()->setName($name);

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->translate()->getName();
    }

    public function translate($locale = null)
    {
        if (null === $locale) {
            $locale = $this->currentLocale;
        }

        if (!$locale) {
            //TODO: change it
            $locale = "ru";
        }

        if ($this->currentTranslation && $this->currentTranslation->getLocale() === $locale) {
            return $this->currentTranslation;
        }

        if (!$translation = $this->translations->get($locale)) {
            $translation = new CityTranslation();
            $translation->setLocale($locale);
            $this->addTranslation($translation);
        }

        $this->currentTranslation = $translation;
        return $translation;
    }

    public static function getTranslationEntityClass()
    {
        return 'CityTranslation';
    }

    public function __toString()
    {
            return $this->getName() != null?$this->getName():$this->translate('ru')->getName();
    }


    /**
     * Add hotels
     *
     * @param \Trans\MainBundle\Entity\Hotel $hotels
     * @return City
     */
    public function addHotel(\Trans\MainBundle\Entity\Hotel $hotels)
    {
        $this->hotels[] = $hotels;

        return $this;
    }

    /**
     * Remove hotels
     *
     * @param \Trans\MainBundle\Entity\Hotel $hotels
     */
    public function removeHotel(\Trans\MainBundle\Entity\Hotel $hotels)
    {
        $this->hotels->removeElement($hotels);
    }

    /**
     * Get hotels
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHotels()
    {
        return $this->hotels;
    }
}
