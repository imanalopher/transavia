<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/25/14
 * Time: 2:24 PM
 */

namespace Trans\MainBundle\Entity;


use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("hotel_translation")
 * @ORM\Entity
 */
class HotelTranslation extends AbstractTranslation
{

    /**
     * @Prezent\Translatable(targetEntity="Trans\MainBundle\Entity\Hotel")
     */
    protected $translatable;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="text")
     */
    private $shortDescription;

    /**
     * Set name
     *
     * @param string $name
     * @return Hotel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     * @return Hotel
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public static function getTranslationEntityClass()
    {
        return 'HotelTranslation';
    }
}