<?php

namespace Trans\MainBundle\Entity;

use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("awards_translation")
 * @ORM\Entity
 */
class AwardsTranslation extends AbstractTranslation
{

    /**
     * @Prezent\Translatable(targetEntity="Trans\MainBundle\Entity\Awards")
     */
    protected $translatable;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * Set description
     *
     * @param string $description
     * @return Awards
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function __toString()
    {
        return (string)$this->getId();
    }

    public static function getTranslationEntityClass()
    {
        return 'AwardsTranslation';
    }
}