<?php

namespace Trans\MainBundle\Entity;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslatable;
use Prezent\Doctrine\Translatable\Annotation as Prezent;

/**
 * Hotel
 *
 * @ORM\Table("hotel_hotel")
 * @ORM\Entity
 */
class Hotel extends AbstractTranslatable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var City
     * @ORM\ManyToOne(targetEntity="Trans\MainBundle\Entity\City", inversedBy="hotels")
     */
    private $city;

    /**
     * @var integer
     *
     * @ORM\Column(name="stars", type="smallint")
     */
    private $stars;

    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     */
    private $media;

    /**
     * @var HotelKitchen
     * @ORM\ManyToMany(targetEntity="Trans\MainBundle\Entity\HotelKitchen")
     */
    private $kitchens;

    /**
     * @Prezent\Translations(targetEntity="Trans\MainBundle\Entity\HotelTranslation")
     */
    protected $translations;

    /**
     * @Prezent\CurrentLocale
     */
    private $currentLocale;

    private $currentTranslation;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Hotel
     */
    public function setName($name)
    {
        $this->translate()->setName($name);

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->translate()->getName();
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     * @return Hotel
     */
    public function setShortDescription($shortDescription)
    {
        $this->translate()->setShortDescription($shortDescription);

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->translate()->getShortDescription();
    }

    /**
     * Set stars
     *
     * @param integer $stars
     * @return Hotel
     */
    public function setStars($stars)
    {
        $this->stars = $stars;

        return $this;
    }

    /**
     * Get stars
     *
     * @return integer
     */
    public function getStars()
    {
        return $this->stars;
    }

    public function translate($locale = null)
    {
        if (null === $locale) {
            $locale = $this->currentLocale;
        }

        if (!$locale) {
            //TODO: change it
            $locale = "ru";
        }

        if ($this->currentTranslation && $this->currentTranslation->getLocale() === $locale) {
            return $this->currentTranslation;
        }

        if (!$translation = $this->translations->get($locale)) {
            $translation = new HotelTranslation();
            $translation->setLocale($locale);
            $this->addTranslation($translation);
        }

        $this->currentTranslation = $translation;
        return $translation;
    }

    public static function getTranslationEntityClass()
    {
        return 'HotelTranslation';
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return \Trans\MainBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param \Trans\MainBundle\Entity\City $city
     */
    public function setCity(City $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Set media
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $media
     * @return Hotel
     */
    public function setMedia(\Application\Sonata\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->media;
    }


    /**
     * Add kitchens
     *
     * @param \Trans\MainBundle\Entity\HotelKitchen $kitchens
     * @return Hotel
     */
    public function addKitchen(\Trans\MainBundle\Entity\HotelKitchen $kitchens)
    {
        $this->kitchens[] = $kitchens;

        return $this;
    }

    /**
     * Remove kitchens
     *
     * @param \Trans\MainBundle\Entity\HotelKitchen $kitchens
     */
    public function removeKitchen(\Trans\MainBundle\Entity\HotelKitchen $kitchens)
    {
        $this->kitchens->removeElement($kitchens);
    }

    /**
     * Get kitchens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKitchens()
    {
        return $this->kitchens;
    }
}
