<?php
/*
-----===++[Additional procedures by Pavel Nedelin (soius@soius.kz;tecc@mail.kz)		]++===-----
-----===++[03.10.2006 - 05.10.2006							]++===-----
-----===++[(p) SOIUS Ltd. 2006 (soius@soius.kz)						]++===-----

-----===++[�������������� ��������� Pavel Nedelin (soius@soius.kz;tecc@mail.kz)		]++===-----
-----===++[03.10.2006 - 05.10.2006							]++===-----
-----===++[(p) SOIUS Ltd. 2006 (soius@soius.kz)						]++===-----
*/

// -----===++[Additional procedures start/�������������� ��������� ������]++===-----
class xml {
    // -----===++[Parse XML to ARRAY]++===-----
    // methods:
    // parse($data) - return array in format listed below
    // variables:
    // $data - string: incoming XML
    //
    // Array format:
    // array index:"TAG_"+tagNAME = value: tagNAME
    // example:$array['TAG_BANK'] = "BANK"
    // array index:NAME+"_"+ATTRIBUTE_NAME = value: ATTRIBUTE_VALUE
    // example:$array['BANK_NAME'] = "Kazkommertsbank JSC"
    //
    // -----===++[����� XML � ������]++===-----
    // ������:
    // parse($data) - ���������� ������ � ������� ��������� ����
    // ����������:
    // $data - ������: �������� XML
    //
    // ������ �������:
    // ������ � �������:"TAG_"+������� = ��������: �������
    // ������:$array['TAG_BANK'] = "BANK"
    // ������ � �������:�������+"_"+������������ = ��������: �����������������
    // ������:$array['BANK_NAME'] = "Kazkommertsbank JSC"

    var $parser;
    var $xarray = array();
    var $lasttag;

    function xml()
    {   $this->parser = xml_parser_create();
        xml_set_object($this->parser, &$this);
        xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, true);
        xml_set_element_handler($this->parser, "tag_open", "tag_close");
        xml_set_character_data_handler($this->parser, "cdata");
    }

    function parse($data)
    {
        xml_parse($this->parser, $data);
        ksort($this->xarray,SORT_STRING);
        return $this->xarray;
    }

    function tag_open($parser, $tag, $attributes)
    {
        $this->lasttag = $tag;
        $this->xarray['TAG_'.$tag] = $tag;
        if (is_array($attributes)){
            foreach ($attributes as $key => $value) {
                $this->xarray[$tag.'_'.$key] = $value;
            };
        };
    }

    function cdata($parser, $cdata)
    {	$tag = $this->lasttag;
        $this->xarray[$tag.'_CHARDATA'] = $cdata;
    }

    function tag_close($parser, $tag)
    {}
}