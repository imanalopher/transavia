<?php
namespace Trans\MainBundle\Utils\epay;
/*
-----===++[Additional procedures by Pavel Nedelin (soius@soius.kz;tecc@mail.kz)		]++===-----
-----===++[05.10.2006									]++===-----
-----===++[SOIUS Ltd. 2006 (soius@soius.kz) http://www.soius.kz				]++===-----

-----===++[�������������� ��������� Pavel Nedelin (soius@soius.kz;tecc@mail.kz)		]++===-----
-----===++[05.10.2006									]++===-----
-----===++[SOIUS Ltd. 2006 (soius@soius.kz) http://www.soius.kz				]++===-----
*/


class Process
{
// -----------------------------------------------------------------------------------------------
    public function process_XML($filename, $reparray)
    {
        // -----===++[Process XML template - replaces tags in file to array values]++===-----
        // variables:
        // $filename - string: name of XML template
        // $reparray - array: data to replace
        //
        // XML template tag format:[tag name] example: [MERCHANT_CERTIFICATE_ID]
        //
        // Functionality:Searches file for array index and replaces to value
        // example: in array > $reparray['MERCHANT_CERTIFICATE_ID'] = "12345"
        // before replace: cert_id="[MERCHANT_CERTIFICATE_ID]"
        // after replace: cert_id="12345"
        // if operation successful returns file contents with replaced values
        // if template not found returns "[ERROR]"
        //
        // -----===++[��������� XML ������� - ������ ����� � ����� �� �������� �� �������]++===-----
        // ����������:
        // $filename - ������: ��� XML �������
        // $reparray - ������: ������ ��� ������
        //
        // ������ ����� � XML �������:[tag name] ������: [MERCHANT_CERTIFICATE_ID]
        //
        // ����������������: ���� � ������� ������� ������� � �������� �� �� ��������
        // ������: � ������� > $reparray['MERCHANT_CERTIFICATE_ID'] = "12345"
        // ����� �������: cert_id="[MERCHANT_CERTIFICATE_ID]"
        // ����� ������: cert_id="12345"
        // ���� �������� ������ ������� ���������� ����� ����� � ����������� ����������
        // ���� ���� ������� �� ����� ���������� "[ERROR]"

        if (is_file($filename)) {
            $content = file_get_contents($filename);
            foreach ($reparray as $key => $value) {
                $content = str_replace("[" . $key . "]", $value, $content);
            };
            return $content;
        } else {
            return "[ERROR]";
        }
    }

// -----------------------------------------------------------------------------------------------
    public function split_sign($xml, $tag)
    {
        // -----===++[Process XML string to array of values]++===-----
        // variables:
        // $xml - string: xml string
        // $tag - string: split tag name
        // $array["LETTER"] = an XML section enclosed in <$tag></$tag>
        // $array["SIGN"] = an XML sign section enclosed in <$tag+"_sign"></$tag+"_sign">
        // $array["RAWSIGN"] = an XML sign section with stripped <$tag+"_sign"></$tag+"_sign"> tags
        // example:
        // income data:
        // $xml = "<order order_id="12345"><department amount="10"/></order><order_sign type="SHA/RSA">ljkhsdfmnuuewrhkj</order_sign>"
        // $tag = "ORDER"
        // result:
        // $array["LETTER"] = "<order order_id="12345"><department amount="10"/></order>"
        // $array["SIGN"] = "<order_sign type="SHA/RSA">ljkhsdfmnuuewrhkj</order_sign>"
        // $array["RAWSIGN"] = "ljkhsdfmnuuewrhkj"
        //
        // -----===++[��������� XML ������ � ��������������]++===-----
        // ����������:
        // $xml - ������: xml ������
        // $tag - ������: ��� ���� �����������
        // $array["LETTER"] = XML ������ ����������� � <$tag></$tag>
        // $array["SIGN"] = XML ������ ������� ����������� � <$tag+"_sign"></$tag+"_sign">
        // $array["RAWSIGN"] = XML ������ ������� � ����������� <$tag+"_sign"></$tag+"_sign"> ������
        // ������:
        // ������� ������:
        // $xml = "<order order_id="12345"><department amount="10"/></order><order_sign type="SHA/RSA">ljkhsdfmnuuewrhkj</order_sign>"
        // $tag = "ORDER"
        // ���������:
        // $array["LETTER"] = "<order order_id="12345"><department amount="10"/></order>"
        // $array["SIGN"] = "<order_sign type="SHA/RSA">ljkhsdfmnuuewrhkj</order_sign>"
        // $array["RAWSIGN"] = "ljkhsdfmnuuewrhkj"


        $array = array();
        $letterst = stristr($xml, "<" . $tag);
        $signst = stristr($xml, "<" . $tag . "_SIGN");
        $signed = stristr($xml, "</" . $tag . "_SIGN");
        $doced = stristr($signed, ">");
        $array['LETTER'] = substr($letterst, 0, -strlen($signst));
        $array['SIGN'] = substr($signst, 0, -strlen($doced) + 1);
        $rawsignst = stristr($array['SIGN'], ">");
        $rawsigned = stristr($rawsignst, "</");
        $array['RAWSIGN'] = substr($rawsignst, 1, -strlen($rawsigned));
        return $array;
    }

// -----------------------------------------------------------------------------------------------
    public function process_request($order_id, $currency_code, $amount, $config_file, $b64 = true)
    {
        // -----===++[Process incoming data to full bank request]++===-----
        // variables:
        // $order_id - integer: order index - recoded to 6 digit format with leaded zero
        // $currency_code - string: preferred currency codes 840-USD, 398-Tenge
        // $amount - integer: total payment amount
        // $config_file - string: full path to config file
        // $b64 - boolean: flag to encode result in base64 default = true
        // example:
        // income data: process_request(1,"398",10,"config.txt")
        // result:
        // string = "<document><merchant cert_id="123" name="test"><order order_id="000001" amount="10" currency="398">
        // <department merchant_id="12345" amount="10"/></order></merchant><merchant_sign type="RSA">LJlkjkLHUgkjhgmnYI</merchant_sign>
        // </document>"
        //
        // -----===++[��������� ������� ������ � ������ ���������� ������]++===-----
        // ����������:
        // $order_id - �����: ����� ������ - �������������� � 6 ������� ������ � �������� ������
        // $currency_code - ������: �������� ����� ����� 840-USD, 398-Tenge
        // $amount - �����: ����� ����� �������
        // $config_file - ������: ������ ���� � ����� ������������
        // $b64 - �������: ���� ��� ����������� ���������� � base64 �� ��������� = true
        // ������:
        // ������� ������: process_request(1,"398",10,"config.txt")
        // ���������:
        // ������ = "<document><merchant cert_id="123" name="test"><order order_id="000001" amount="10" currency="398">
        // <department merchant_id="12345" amount="10"/></order></merchant><merchant_sign type="RSA">LJlkjkLHUgkjhgmnYI</merchant_sign>
        // </document>"


        $epay_root = $config_file;
        $config_file .='config.dat';
        if (is_file($config_file)) {
            $config = parse_ini_file($config_file, 0);
        } else {
            return "Config not exist";
        }

        if (strlen($order_id) > 0) {
            if (is_numeric($order_id)) {
                if ($order_id > 0) {
                    $order_id = sprintf("%06d", $order_id);
                } else {
                    return "Null Order ID";
                };
            } else {
                return "Order ID must be number";
            };
        } else {
            return "Empty Order ID";
        }
        if (strlen($currency_code) == 0) {
            return "Empty Currency code";
        }
        if ($amount == 0) {

            return "Nothing to charge";
        }
        if (strlen($config['PRIVATE_KEY_FN']) == 0) {
            return "Path for Private key not found";
        }
        if (strlen($config['XML_TEMPLATE_FN']) == 0) {
            return "Path for Private key not found";
        }

        $config['PRIVATE_KEY_FN'] = $epay_root . $config['PRIVATE_KEY_FN'];
        $config['XML_TEMPLATE_FN'] = $epay_root . $config['XML_TEMPLATE_FN'];
        $config['PUBLIC_KEY_FN'] = $epay_root . $config['PUBLIC_KEY_FN'];

        $request = array();
        $request['MERCHANT_CERTIFICATE_ID'] = $config['MERCHANT_CERTIFICATE_ID'];
        $request['MERCHANT_NAME'] = $config['MERCHANT_NAME'];
        $request['ORDER_ID'] = $order_id;
        $request['CURRENCY'] = $currency_code;
        $request['MERCHANT_ID'] = $config['MERCHANT_ID'];
        $request['AMOUNT'] = $amount;


        $kkb = new KKBsign();
        $kkb->invert();
        if (!$kkb->load_private_key($config['PRIVATE_KEY_FN'], $config['PRIVATE_KEY_PASS'])) {
            if ($kkb->ecode > 0) {
                return $kkb->estatus;
            }
        }
        $result = $this->process_XML($config['XML_TEMPLATE_FN'], $request);
        if (strpos($result, "[RERROR]") > 0) {
            return "Error reading XML template.";
        }
        $result_sign = '<merchant_sign type="RSA">' . $kkb->sign64($result) . '</merchant_sign>';
        $xml = "<document>" . $result . $result_sign . "</document>";
        if ($b64) {
            return base64_encode($xml);
        } else {
            return $xml;
        }
    }

// -----------------------------------------------------------------------------------------------
    public function process_response($response, $config_file)
    {
        // -----===++[Process incoming XML to array of values with verifying electronic sign]++===-----
        // variables:
        // $response - string: XML response from bank
        // $config_file - string: full path to config file
        // returns:
        // array with parced XML and sign verifying result
        // if array has in values "DOCUMENT" following values available
        // $data['CHECKRESULT'] = "[SIGN_GOOD]" - sign verify successful
        // $data['CHECKRESULT'] = "[SIGN_BAD]" - sign verify unsuccessful
        // $data['CHECKRESULT'] = "[SIGN_CHECK_ERROR]" - an error has occured while sign processing full error in that string after ":"
        // if array has in values "ERROR" following values available
        // $data["ERROR_TYPE"] = "ERROR" - internal error occured
        // $data["ERROR"] = "Config not exist" - the configuration file not found
        // $data["ERROR_TYPE"] = "system" - external error in bank process
        // $data["ERROR_TYPE"] = "auth" - external autentication error in bank process
        // example:
        // income data:
        // $response = "<document><bank><customer name="123"><merchant name="test merch">
        // <order order_id="000001" amount="10" currency="398"><department amount="10"/></order></merchant>
        // <merchant_sign type="RSA"/></customer><customer_sign type="RSA"/><results timestamp="2001-01-01 00:00:00">
        // <payment amount="10" response_code="00"/></results></bank>
        // <bank_sign type="SHA/RSA">;skljfasldimn,samdbfyJHGkmbsa;fliHJ:OIUHkjbn</bank_sign ></document>"
        // $config_file = "config.txt"
        // result:
        // $data['BANK_SIGN_CHARDATA'] = ";skljfasldimn,samdbfyJHGkmbsa;fliHJ:OIUHkjbn"
        // $data['BANK_SIGN_TYPE'] = "SHA/RSA"
        // $data['CUSTOMER_NAME'] = "123"
        // $data['CUSTOMER_SIGN_TYPE'] = "RSA"
        // $data['DEPARTMENT_AMOUNT'] = "10"
        // $data['MERCHANT_NAME'] = "test merch"
        // $data['MERCHANT_SIGN_TYPE'] = "RSA"
        // $data['ORDER_AMOUNT'] = "10"
        // $data['ORDER_CURRENCY'] = "398"
        // $data['ORDER_ORDER_ID'] = "000001"
        // $data['PAYMENT_AMOUNT'] = "10"
        // $data['PAYMENT_RESPONSE_CODE'] = "00"
        // $data['RESULTS_TIMESTAMP'] = "2001-01-01 00:00:00"
        // $data['TAG_BANK'] = "BANK"
        // $data['TAG_BANK_SIGN'] = "BANK_SIGN"
        // $data['TAG_CUSTOMER'] = "CUSTOMER"
        // $data['TAG_CUSTOMER_SIGN'] = "CUSTOMER_SIGN"
        // $data['TAG_DEPARTMENT'] = "DEPARTMENT"
        // $data['TAG_DOCUMENT'] = "DOCUMENT"
        // $data['TAG_MERCHANT'] = "MERCHANT"
        // $data['TAG_MERCHANT_SIGN'] = "MERCHANT_SIGN"
        // $data['TAG_ORDER'] = "ORDER"
        // $data['TAG_PAYMENT'] = "PAYMENT"
        // $data['TAG_RESULTS'] = "RESULTS"
        // $data['CHECKRESULT'] = "[SIGN_GOOD]"
        //
        // -----===++[������������������ XML � ������ �������� � ��������� ����������� �������]++===-----
        // ����������:
        // $response - ������: XML ����� �� �����
        // $config_file - ������: ������ ���� � ����� ������������
        // ����������:
        // ������ � ���������� XML � ����������� �������� �������
        // ���� � ������� ���� �������� "DOCUMENT" �������� ��������� ��������
        // $data['CHECKRESULT'] = "[SIGN_GOOD]" - �������� ������� �������
        // $data['CHECKRESULT'] = "[SIGN_BAD]" - �������� ������� ���������
        // $data['CHECKRESULT'] = "[SIGN_CHECK_ERROR]" - ��������� ������ �� ����� ��������� �������, ������ �������� ������ � ���� �� ������ ����� ":"
        // ���� � ������� ���� �������� "ERROR" �������� ��������� ��������
        // $data["ERROR_TYPE"] = "ERROR" - ��������� ���������� ������
        // $data["ERROR"] = "Config not exist" - �� ������ ���� ������������
        // $data["ERROR_TYPE"] = "system" - ������� ������ ��� ��������� ������ � �����
        // $data["ERROR_TYPE"] = "auth" - ������� ������ ����������� ��� ��������� ������ � �����
        // ������:
        // ������� ������:
        // $response = "<document><bank><customer name="123"><merchant name="test merch">
        // <order order_id="000001" amount="10" currency="398"><department amount="10"/></order></merchant>
        // <merchant_sign type="RSA"/></customer><customer_sign type="RSA"/><results timestamp="2001-01-01 00:00:00">
        // <payment amount="10" response_code="00"/></results></bank>
        // <bank_sign type="SHA/RSA">;skljfasldimn,samdbfyJHGkmbsa;fliHJ:OIUHkjbn</bank_sign ></document>"
        // $config_file = "config.txt"
        // ���������:
        // $data['BANK_SIGN_CHARDATA'] = ";skljfasldimn,samdbfyJHGkmbsa;fliHJ:OIUHkjbn"
        // $data['BANK_SIGN_TYPE'] = "SHA/RSA"
        // $data['CUSTOMER_NAME'] = "123"
        // $data['CUSTOMER_SIGN_TYPE'] = "RSA"
        // $data['DEPARTMENT_AMOUNT'] = "10"
        // $data['MERCHANT_NAME'] = "test merch"
        // $data['MERCHANT_SIGN_TYPE'] = "RSA"
        // $data['ORDER_AMOUNT'] = "10"
        // $data['ORDER_CURRENCY'] = "398"
        // $data['ORDER_ORDER_ID'] = "000001"
        // $data['PAYMENT_AMOUNT'] = "10"
        // $data['PAYMENT_RESPONSE_CODE'] = "00"
        // $data['RESULTS_TIMESTAMP'] = "2001-01-01 00:00:00"
        // $data['TAG_BANK'] = "BANK"
        // $data['TAG_BANK_SIGN'] = "BANK_SIGN"
        // $data['TAG_CUSTOMER'] = "CUSTOMER"
        // $data['TAG_CUSTOMER_SIGN'] = "CUSTOMER_SIGN"
        // $data['TAG_DEPARTMENT'] = "DEPARTMENT"
        // $data['TAG_DOCUMENT'] = "DOCUMENT"
        // $data['TAG_MERCHANT'] = "MERCHANT"
        // $data['TAG_MERCHANT_SIGN'] = "MERCHANT_SIGN"
        // $data['TAG_ORDER'] = "ORDER"
        // $data['TAG_PAYMENT'] = "PAYMENT"
        // $data['TAG_RESULTS'] = "RESULTS"
        // $data['CHECKRESULT'] = "[SIGN_GOOD]"


        if (is_file($config_file)) {
            $config = parse_ini_file($config_file, 0);
        } else {
            $data["ERROR"] = "Config not exist";
            $data["ERROR_TYPE"] = "ERROR";
            return $data;
        };

        $xml_parser = new \xml();
        $result = $xml_parser->parse($response);
        if (in_array("ERROR", $result)) {
            return $result;
        };
        if (in_array("DOCUMENT", $result)) {
            $kkb = new \KKBSign();
            $kkb->invert();
            $data = $this->split_sign($response, "BANK");
            $check = $kkb->check_sign64($data['LETTER'], $data['RAWSIGN'], $config['PUBLIC_KEY_FN']);
            if ($check == 1)
                $data['CHECKRESULT'] = "[SIGN_GOOD]";
            elseif ($check == 0)
                $data['CHECKRESULT'] = "[SIGN_BAD]";
            else
                $data['CHECKRESULT'] = "[SIGN_CHECK_ERROR]: " . $kkb->estatus;
            return array_merge($result, $data);
        };
        return "[XML_DOCUMENT_UNKNOWN_TYPE]";
    }
// -----===++[Additional procedures end/�������������� ��������� �����]++===-----
}

?>
