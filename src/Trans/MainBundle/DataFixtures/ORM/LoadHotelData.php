<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/25/14
 * Time: 9:04 PM
 */

namespace Trans\MainBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\MainBundle\Entity\Hotel;

class LoadHotelData extends AbstractFixture implements OrderedFixtureInterface
{


    function load(ObjectManager $manager)
    {
        $values = array(
            array('name' => 'Тобол',
                'shortDescription' => 'домашний уют и приятный сервис',
                'stars' => 4,
                'kitchen' => $this->getReference('национальная'),
                'city' => $this->getReference('Кордай')),
            array('name' => 'Медеу',
                'shortDescription' => 'высокое качество комфорта и свежий воздух',
                'stars' => 5,
                'kitchen' => $this->getReference('европейская'),
                'city' => $this->getReference('Кордай')),
            array('name' => 'Айдана',
                'shortDescription' => 'высокое качество комфорта и приятный сервис',
                'stars' => 5,
                'kitchen' => $this->getReference('турецкая'),
                'city' => $this->getReference('Астана'))
        );

        foreach ($values as $value) {
            $hotel = new Hotel();
            $hotel->setName($value['name'])
                ->setShortDescription($value['shortDescription'])
                ->setStars($value['stars'])
                ->setCity($value['city'])
                ->addKitchen($value['kitchen']);
            $manager->persist($hotel);
        }
        $manager->flush();

    }

    function getOrder()
    {
        return 2;
    }
}