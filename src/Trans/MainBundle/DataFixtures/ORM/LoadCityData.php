<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/25/14
 * Time: 3:07 PM
 */

namespace Trans\MainBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\MainBundle\Entity\City;

class LoadCityData extends AbstractFixture implements OrderedFixtureInterface
{

    function load(ObjectManager $manager)
    {
        $values = array('Кордай', 'Астана', 'Алматы');
        foreach ($values as $value) {
            $city = new City();
            $city->setName($value);
            $manager->persist($city);
            $this->addReference($value, $city);
        }
        $manager->flush();
    }

    function getOrder()
    {
        return 1;
    }
}