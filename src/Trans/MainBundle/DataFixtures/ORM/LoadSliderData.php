<?php
/**
 * Created by PhpStorm.
 * User: isabek
 * Date: 06.06.14
 * Time: 22:32
 */

namespace Trans\MainBundle\DataFixtures\ORM;


use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\MainBundle\Entity\Slide;

class LoadSliderData extends AbstractFixture implements OrderedFixtureInterface
{
    function load(ObjectManager $manager)
    {
        $images = array(
            array(
                'title' => 'в любую точку мира! быстро и легко, вместе с нами',
                'name' => 'slide1.png'
            ),
            array(
                'title' => 'в любую точку мира! быстро и легко, вместе с нами',
                'name' => 'slide2.png'
            ),
            array(
                'title' => 'в любую точку мира! быстро и легко, вместе с нами',
                'name' => 'slide3.png'
            )
        );

        foreach ($images as $image) {
            $slide = new Slide();
            $slide
                ->setMedia($this->generateMediaByFileName($image['name']))
                ->setActive(true)
                ->translate('ru')
                ->setTitle($image['title']);
            $manager->persist($slide);
        }

        $manager->flush();
    }

    private function getImageDirByName($name)
    {
        return sprintf(__DIR__ . '/../Resources/images/%s', $name);
    }

    private function generateMediaByFileName($name)
    {
        $image = new Media();
        $image->setProviderName('sonata.media.provider.image');
        $image->setContext('slide');
        $image->setBinaryContent($this->getImageDirByName($name));

        return $image;
    }

    function getOrder()
    {
        return 1;
    }
}