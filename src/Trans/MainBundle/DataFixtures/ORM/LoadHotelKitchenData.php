<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/25/14
 * Time: 3:50 PM
 */

namespace Trans\MainBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\MainBundle\Entity\HotelKitchen;

class LoadHotelKitchenData extends AbstractFixture implements OrderedFixtureInterface
{


    function load(ObjectManager $manager)
    {
        $values = array('национальная', 'европейская', 'турецкая', 'корейская');
        foreach ($values as $value) {
            $kitchen = new HotelKitchen();
            $kitchen->setName($value);
            $manager->persist($kitchen);
            $this->addReference($value, $kitchen);
        }
        $manager->flush();

    }


    function getOrder()
    {
        return 0;
    }
}