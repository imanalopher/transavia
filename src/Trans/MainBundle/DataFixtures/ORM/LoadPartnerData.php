<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/26/14
 * Time: 2:48 AM
 */

namespace Trans\MainBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Trans\MainBundle\Entity\Partner;

class LoadPartnerData extends AbstractFixture implements OrderedFixtureInterface
{


    function load(ObjectManager $manager)
    {
        $values = array(array(
            'name' => 'Аэрофлот',
            'shortDescription' => 'Российские авиалинии',
            'url' => 'http://wwww.aeroflot.ru'),
            array(
                'name' => 'Бритиш Эйрвейс',
                'shortDescription' => 'British Airways is a full service global airline, offering year-round low fares with an extensive global route network',
                'url' => 'http://www.britishairways.com'
            ),
            array(
                'name' => 'Эйр астана',
                'shortDescription' => 'From the heart of euroasia',
                'url' => 'http://www.airastana.com'
            )
        );

        foreach ($values as $value) {
            $partner = new Partner();
            $partner->setName($value['name'])
                ->setShortDescription($value['shortDescription'])
                ->setUrl($value['url']);
            $manager->persist($partner);
        }
        $manager->flush();
    }


    function getOrder()
    {
        return 0;
    }
}