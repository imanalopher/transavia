<?php
/**
 * Created by PhpStorm.
 * User: Nurolopher
 * Date: 21.06.14
 * Time: 17:34
 */

namespace Trans\MainBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class EpayType  extends AbstractType    {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('reservationCode','text',array('attr' => array('class' => 'b-input__text b-fullname__input')))
            ->add('email','email', array('required' => true,
                'attr' => array('class' => 'b-input__text b-email__input',
                )))
            ->add('telephone','text',array('required' => true,
                'attr' => array('class' => 'b-input__text b-tel__input')))
            ->add('airfare','number',array('attr' => array('class' => 'b-input__text b-fullname__input')))
            ->add('submit','submit',array('label'=>'Pay','attr' => array('class' => 'b-button')))
        ;
    }


    public function getName()
    {
        return 'epay';
    }
}