<?php

namespace Trans\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AwardsController extends Controller
{

    public function indexAction()
    {
        $awards = $this->get('doctrine.orm.entity_manager')
            ->getRepository('TransMainBundle:Awards')
            ->findAll();

        return $this->render('TransMainBundle:Awards:index.html.twig', array('awards' => $awards));
    }

    public function randomFiveAwardsAction()
    {
        $awards = $this->get('doctrine.orm.entity_manager')
            ->getRepository('TransMainBundle:Awards')
            ->createQueryBuilder('a')->setMaxResults(5)->getQuery();

        return $this->render('TransMainBundle:Awards:randomFiveAwards.html.twig', array('awards' => $awards->getResult()));
    }

} 