<?php
/**
 * Created by PhpStorm.
 * User: Nurolopher
 * Date: 21.06.14
 * Time: 17:38
 */

namespace Trans\MainBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Trans\MainBundle\Form\Type\EpayType;
use Trans\MainBundle\Utils\epay;

class EpayController extends Controller {

    public function indexAction(Request $request){

        $form = $this->createForm(new EpayType());

        if($request->isMethod('POST')){

            $epay = $request->request->get('epay');

            $path = $this->container->get('kernel')->getRootdir().'/../src/Trans/MainBundle/Utils/epay/' ;

            $order_id = date("dmYHi").(int)stripslashes(htmlspecialchars($epay['reservationCode']));

            $currency_id = "398";

            $amount = (int)stripslashes(htmlspecialchars($epay['airfare']));

            $process  = new epay\Process();

            $Base64Content = $process->process_request($order_id, $currency_id, $amount, $path);

            $epay['date'] = new \DateTime('now');

            $this->sendMail($epay);

            return $this->render('TransMainBundle:Epay:order.html.twig',array('base64Content'=>$Base64Content));
        }
        return $this->render('TransMainBundle:Epay:index.html.twig',array('form'=>$form->createView()));
    }

    private function sendMail($data)
    {
        $receipents = array(
            $this->container->getParameter('mailer_admin'),
            $this->container->getParameter('mailer_sales'),
            $this->container->getParameter('mailer_sales_online'),
            $this->container->getParameter('mailer_matveev'),
            $this->container->getParameter('mailer_booking'),
        );
        $body = $this->renderView('TransMainBundle:Epay:message.html.twig',array('data'=>$data));
        foreach ($receipents as $receipent) {
            $message = \Swift_Message::newInstance()
                ->setCharset('utf-8')
                ->setContentType('text/html')
                ->setSubject('Онлайн оплата')
                ->setFrom($this->get('service_container')->getParameter('mailer_user'))
                ->setTo($receipent)
                ->setBody($body);

            $this->get('mailer')->send($message);
        }
    }
} 