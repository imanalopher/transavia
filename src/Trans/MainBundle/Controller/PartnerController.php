<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/26/14
 * Time: 2:42 AM
 */

namespace Trans\MainBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Trans\MainBundle\Entity\Partner;

class PartnerController extends Controller
{

    public function indexAction()
    {
        $partners = $this->get('doctrine.orm.entity_manager')
            ->getRepository('TransMainBundle:Partner')
            ->findAll();

        return $this->render('TransMainBundle:Partner:index.html.twig', array('partners' => $partners));
    }

    public function showAction(Partner $partner)
    {
        if (!$partner)
            $this->createNotFoundException('Партнер не найден');
        return $this->render('TransMainBundle:Partner:show.html.twig',array('partner'=>$partner));
    }

} 