<?php

namespace Trans\MainBundle\Controller;


use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Trans\PartialBundle\Entity\Partial;
use Trans\PartialBundle\Entity\Section;

class ContactController extends Controller
{
    public function indexAction(Request $request)
    {
        $partialId = $request->get('partial', Partial::CENTRAL_OFFICE);

        $registry = $this->getDoctrine();

        $section = $registry->getRepository('TransPartialBundle:Section')->findOneBy(array(
            'name' => Section::CONTACTS_NAME
        ));

        if (!$section) {
            throw new EntityNotFoundException();
        }

        $partialRepository = $registry->getRepository('TransPartialBundle:Partial');

        $contacts = $partialRepository->findBy(array(
            'section' => $section
        ));

        $contact = $partialRepository->findOneBy(array(
            'partial_id' => $partialId
        ));

        if (!$contacts || !$contact || $contact->getSection()->getId() != $section->getId()) {
            throw new EntityNotFoundException();
        }

        return $this->render('@TransMain/Contact/index.html.twig', array(
            'contacts' => $contacts,
            'chosen' => $contact
        ));
    }
} 