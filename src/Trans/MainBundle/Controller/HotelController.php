<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/25/14
 * Time: 3:29 PM
 */

namespace Trans\MainBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HotelController extends Controller
{

    public function indexAction()
    {
        $cities = $this->get('doctrine.orm.entity_manager')->getRepository('TransMainBundle:City')->findAll();

        return $this->render('TransMainBundle:Hotel:index.html.twig', array('cities' => $cities));
    }
} 