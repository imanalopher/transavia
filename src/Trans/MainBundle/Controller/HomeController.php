<?php

namespace Trans\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Trans\TicketBundle\Entity\OrderHotel;
use Trans\TicketBundle\Entity\OrderTrain;
use Trans\TicketBundle\Form\Type\OrderHotelType;
use Trans\TicketBundle\Form\Type\OrderTrainType;

class HomeController extends Controller
{
    public function indexAction()
    {
        $slides = $this->getDoctrine()->getRepository('TransMainBundle:Slide')->findBy(array(
            'active' => true
        ));

        $formHotel = $this->createForm(new OrderHotelType($this->get('translator')),new OrderHotel(), array('action' => $this->generateUrl('order_hotel_new')));
        $formTrain = $this->createForm(new OrderTrainType($this->get('translator')),new OrderTrain(), array('action' => $this->generateUrl('order_train_new')));
        return $this->render('TransMainBundle:Home:index.html.twig', array(
            'slides' => $slides,
            'formHotel' => $formHotel->createView(),
            'formTrain' => $formTrain->createView()
        ));
    }

    public function scheduleAction()
    {
        $stationId = $this->get('service_container')->getParameter('station_id');

        $formHotel = $this->createForm(new OrderHotelType($this->get('translator')),new OrderHotel(), array('action' => $this->generateUrl('order_hotel_new')));
        $formTrain = $this->createForm(new OrderTrainType($this->get('translator')),new OrderTrain(), array('action' => $this->generateUrl('order_train_new')));

        return $this->render('@TransMain/Home/schedule.html.twig', array(
            'station' => $stationId,
            'formHotel' => $formHotel->createView(),
            'formTrain' => $formTrain->createView()
        ));
    }
}
