<?php
namespace Trans\MainBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AwardsAdmin extends Admin
{

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('translations', 'a2lix_translations')
            ->add('media')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                    'show' => array()
                )
            ));
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('translations', 'a2lix_translations', array(
                'fields' => array(
                    'description' => array(
                        'field_type' => 'textarea',
                        'label' => 'Описание',
                        'required' => false,
                        'attr' => array('rows'=>5, 'cols'=> 60)
                    )
                )
            ))
            ->add('media', 'sonata_type_model_list', array('required'=> true, 'label'=> 'Галлерея'), array('link_parameters'=>array('context'=>'awards')));
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter->add('translations', 'a2lix_translations')
            ->add('description')
            ->add('media');
    }
} 