<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/25/14
 * Time: 2:47 PM
 */

namespace Trans\MainBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class SlideAdmin extends Admin
{

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('translations', 'a2lix_translations')
            ->add('title')
            ->add('active', null, array(
                'editable' => true
            ));
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('media', 'sonata_media_type', array(
                'provider' => 'sonata.media.provider.image',
                'context' => 'slide',
                'required' => false
            ))
            ->add('active', null, array('required' => false))
            ->add('translations', 'a2lix_translations', array(
                'fields' => array(
                    'title' => array('label' => 'Название')
                )
            ));

    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter->add('translations', 'a2lix_translations')
            ->add('title')
            ->add('active');
    }


} 