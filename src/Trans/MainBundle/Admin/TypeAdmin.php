<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/25/14
 * Time: 3:16 PM
 */

namespace Trans\MainBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TypeAdmin extends Admin
{

    protected function configureFormFields(FormMapper $form)
    {
        $form->add('translations', 'a2lix_translations', array(
            'fields' => array('name' => array('label' => 'Название'))
        ));
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('translations', 'a2lix_translations', array('label' => 'Название'));
    }


} 