<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/25/14
 * Time: 2:47 PM
 */

namespace Trans\MainBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class HotelAdmin extends Admin
{

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('translations', 'a2lix_translations')
            ->add('city')
            ->add('stars')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                    'show' => array()
                )
            ));
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('city', null, array('required' => true))
            ->add('translations', 'a2lix_translations', array(
                'fields' => array(
                    'name' => array('label' => 'Название')
                )
            ))
            ->add('media', 'sonata_media_type', array(
                'provider' => 'sonata.media.provider.image',
                'context' => 'hotel',
                'required' => false
            ))
            ->add('stars')
            ->add('kitchens');
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter->add('translations', 'a2lix_translations')
            ->add('shortDescription')
            ->add('city')
            ->add('kitchens')
            ->add('stars');
    }


} 