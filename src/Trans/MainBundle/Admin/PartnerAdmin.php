<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 5/25/14
 * Time: 2:47 PM
 */

namespace Trans\MainBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PartnerAdmin extends Admin
{

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('translations', 'a2lix_translations')
            ->add('url')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                    'show' => array()
                )
            ));
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('translations', 'a2lix_translations', array(
                'fields' => array(
                    'name' => array('label' => 'Название'),
                    'shortDescription' => array(
                        'field_type' => 'ckeditor',
                        'label' => 'Короткое описание'
                    ),
                    'content' => array(
                        'field_type' => 'ckeditor',
                        'label' => 'Контент'
                    )
                )
            ))
            ->add('media', 'sonata_media_type', array(
                'provider' => 'sonata.media.provider.image',
                'context' => 'partner',
                'required' => false
            ))
            ->add('url', 'url', array('required' => true));
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter->add('translations', 'a2lix_translations')
            ->add('shortDescription')
            ->add('media')
            ->add('url');
    }


} 